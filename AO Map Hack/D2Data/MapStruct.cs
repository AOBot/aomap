﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace AO_Map_Client
{
    [Serializable]
    public class MapStruct
    {
        public AreaLevel area;
        public uint width;
        public uint height;
        public byte[] mapdata;
    
    }

    [Serializable]
    public class Exit
    {
        public AreaLevel from;
        public AreaLevel to;
        public Point location;
    }

    [Serializable]
    public class Npc
    {
        public AreaLevel area;
        public int npcid;
        public Point location;            
    }

    [Serializable]
    public class Object
    {
        public AreaLevel area;
        public int objectid;
        public Point location;   
    }
}
