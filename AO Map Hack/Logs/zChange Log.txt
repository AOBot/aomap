﻿messing with drawing such as the rotation part
trying to get the character follow to work with 
perspective mode

added in precision drawing so you can change
wether it's sampling/smoothing/interpolation mode

R52 4/3/10 - CTS
	- Hopefully fixed window topmost problems
	- created a new winSet --> winSetShowV2
	4/4/10 - CTS
	- followingWindow issues should be fixed now :D w00000t!
	- widnow control has more logic now
	
R51 3/22/10 - CTS
	- Fixed small caching issue
	- Added new gfx for waypoint, not in use yet though
	
R50 2/17/10 - CTS
	- messed with drawing such as sampling/interpoling and smoothing/AntiAliasing
	- set window name to "Diablo II     ID: "+ID
	3/18/10 - CTS
	- fixed up the rotating stuff
	- made drawing a bit more efficient, now goes from map size instead of window size
	- really close for the perspective center char, but not close enough...

R49 1/12/10 - CTS
	- whole program is see through now; no black back
	- added get window state to the wincontrol (multi thread, grrr lol)
	- fixed some of the new problems with window activation

R48 1/4/10 - CTS
	- fixed problem with centerchar and exits drawing

R47 11-15~17-09 - CTS
	- reworked the rotation correctly
		- though old things need fixing to the new system now
	- character position now resides in it's own window 
	- Top most window doesn't have anything drawing inside of it now.
	- added center character
		- non perspective mode works so far
	- optimized some of the drawing

R46 10-21-09 - CTS
	- added dithering, c'est fantastique! :D

R45 10-21-09 - CTS
	- improved how exit drawing is handled (draws on one form which never is disposed of)
	-- fixed the blackout focus bug with the new exit drawing method
	-- exit drawing happens much much much more faster
	- not much testing but lets hope for the best : )

R44 10-17-09 - CTS
	- quick fix on map logic, it would draw some maps too soon
	- also my weird dithering tests are making it into this svn commit

R43 10-16-09 - CTS
	- all foreach form overlays are put into a try catch now, which will tell what section it was thrown in
	- fixed (possibly) a reason for crashes, I was disposing of a layer w/o removing it from the overlay array
		thus causing problems later when calling on the foreach loop for overlays
	- added in some disposing extras to the overlay to remove the object from the array list : )
	- changed caching so it all fits under the caching class instead of separate classes
		now one save/load can do all the saving/loading : )
	- updated all caching to new caching class (cleaner less redundant source (more exciting cache class though :P )
	- created some huge major crashing issue, BUT
	- fixed huge major crashing issue, after an hour of trying
	- realized I need to multi thread the exit drawing, and/or the debug logger
	- auto connection and bring d2 to front : ) if you reply yes to following the detected d2 window
	- added some more logic to the socket events such as map recieved and level changed
		levels shouldn't be redrawn until the map has been received and the map recieved shouldn't be
		drawn until a level change has been initiated ; )
	- in responce to more socket event logic, map drawing seems pretty faster
		(I think it was drawing the old map sometimes before just waiting to draw the new one : ) )
		if not just skipping the new one and not drawing it.
	- black outs seem less common, though I'm going to try one more idea I have

R43 10-15-09 - CTS
	- removed some main window events like when it got deactivated, hopefully this will fix blacking problem
	- fixed? the focus loss on d2, was double showing the exit windows I guess, because
		apparently when you set it to an overlay it shows it? idk.
	- fixed? 3rd time I figured I fixed this... but this time for real? but what did I truly do..
		just rearranged some stuff I guess lol

R42 10-15-09 - CTS
	- removed sendmessenger from AoUtils, was obsolete/not used
	- map chaching seems to work just fine, it will load the past maps from the same game
		but need to flush after game ends (need to get controller events working : ( )
	- Finally! got the controller event args cross threaded correctly now : ))
		And connects to core : )
	- fixed map chaching, should be solid now
	- fixed hopefully pre-drawing maps
	- hides map when in lobby, re-shows when in game

R41 10-14-09 - CTS
	- removed the Form_Data Folder and Re-merged the Events & Functions back into one
	- cleaned up code
	- fixed a toggle issue with following the d2 window
	- fixed some of the text on the buttons to be easier to read and understand
	- changed the settings save location
	- changed some of the default settings such as map opacity and follow mode
	- changed some dialog text
	- fixed a problem whith window following while using the options dialog
	- added show d2 window button (makes life easier and faster :P(specially for my repetition))
	- added serialization (needs to be tested and tuned...)
		just want to commit b4 I screw something up.. lol

R41 10/11/09 - CTS
	- fixed some things in the log :P
	- exits should now update to perspective changing

R40 10/11/09 - CTS
	- added draws exits now
	- fixed a strobing graphics issue when exits were added / level changed
		which I probally created lol
	- dejunked some more, removed useless classes, and some more code
	- random throw catches put in when ever there's a random ish crash
	- fixed problem where it wouldn't load the first map when creating a game
		fully did this time
	- maps load faster, since they load on the receive again, instead
		of just on the change event

R40 10/10/09 - CTS
	- cleaned up some old obsolete events
	- moved the set window to layered mode into the wincontrol class
	- added an enum for the overlays so they can be told from one another
	- created exit cache

R39 10/10/09 - CTS
	- added minimum form values
	- fixed map not following window : )
	- added layered mode windows so clicks send through them
	- fixed map loading area's before entering them
		and will load the first area too
	- added an exit button on the gui
	- fixed show/hide overlay issues
	- fixed (hopefully) the player position should be on top of the map now

R39 10/2/09 - CTS
	- added some grabber/follower options

R38 9/29/09 - CTS
	- removed some more obsolete gui
	- clicks now send left and rights to d2
	- updated/fixed the grab window
	- no hide is no obsolete for this project
		all no hiding is passive now instead of aggressive

R37 9/29/09 - CTS
	- QuickFixes now actually @ R37
	- Removed dev test button1 from gui

R37 9/29/09 - CTS
	- Added now sends messages to D2 window for the mouse position on the overlays
	- Added search system that looks for d2 process, much better than the list view way
		it was painful but well worth it :P
	- Added sending clicks from overlays to d2 window

R36 9/28/09 - CTS
	- quickfix when trying to connect w/o an owner name
		overlay wouldn't turn off correctly or show correctly

R35 9/28/09 - CTS
	- quickfix need to add FollowWindow to SVN

R34 9/28/09 - CTS
	- created FollowWindow
		d2 window now tracks to the map client window THANK GOD :D  W00t!
	- needs to send clicks to d2 when produced on the map client windows
	- fixed get position and get size in WinControl

R33 9/28/09 - CTS
	- quickfix on manifest issue
	- quickfix on processprivledges not being needed

R32 9/28/09 - CTS
	- Added onto WinControl, more things and better
	- fixed some things in no hide and moved some stuff into WinControl
	- fixed bug where it was grabbing just "handle" that screws over the whole pc!
		grab "MainWindowHandle" geesh!

R32 9/27/09 - CTS
	- Added and modified no hide to project
	- Created WinControl in Utils
	
R31 9/26/09 - CTS
	- fixed resizing bug between the main form and the child overlays
		they were referncing a static constructor for the picturebox size
	- early break for 
		"case MapEventPackets.NPC:"
		"case MapEventPackets.OBJECT:"
		so that there's not data useage there for now; un needed
	- change in gui layout ; )

R31 9/24/09 - CTS
	- added overlay toggling
	- fixed! omg figured out that you dont ever want to use black for
		the color key / transparency key...
		fixed some full screen issues with becoming "stuck"
	- restructured Form, with Form_Data for Events and Functions

R30 9/24/09 - CTS
	- fixed some window resizing and state bugs
	- added ejact's sendmessage class

R30 9/10/09 - CTS
	- fixed small object order sequence problem ;)

R29 9/9/09 - CTS
	- fixed a really annoying bug, where the middle layer class wasn't drawing...
		the picturebox was set to false for visibility..... *Face Palm*
		quick and easy fix, but hard to find and well worth it, Thank god... lol

R28 9/2/09 - CTS
	- fixed layer bugs
	- added onto options form
	- npc's are drawn into the overlays, just for debugging purposes right now

R26 8/12/09 - CTS
	- quick update so ejact can work on
	- middle layer window class just about done.

R25 8/11/09 - ejact
	- fixed MapCache
	- increased the receiving speed a lot.

R24	8/10/09 - CTS
	- added options form
	
	8/4/09 - CTS
	- fixed problem in overlay where lower window could be lost behind other windows
	- fixed problem with which layer was focused

	8/3/09 - CTS
	- fixed won't draw if the map struct is null
	- created an overlay form/class - beta
	- moved bitmap controls into it's own class "BitmapControl"
	- created a Drawing Class for map overlay drawing functions
	- Overlay now has a master top layer and lower layers

R20 6/24/09 - CTS
	- fixed when dragging main window debug window wouldn't be on top of other windows
		now gives focus to the debug window so it's on top :)
	- fixed now loads the multi-tone setting at program start

R19 6/24/09 - CTS
	- added a try catch onto the draw function
	- added multi-tone option :D
	- changed layout
	- added gui for core startup and config

R18 6/23/09 - CTS
	- changed how the debug window follows the parent window now
	- removed some useless stuff for now

R17 6/23/09 - ejact
	- added delegate so debug window can be passed to

R16 6/23/09 - CTS
	- changed how debug window functions
	- changed how to write to debug window, just use the global function for child forms
	- changed that map no longer draw when received since they are received when you get near the next level
		only the next level trigger will draw the map
	- debug window now limits the text and shortens the window correctly :D

R14 6/21/09 - CTS
	- debug window
	- tried connection stuff, probally created more of a mess than good lol : (

R13 6/8/09 - ejact
	- maps are being dumped directly in the mapclient, just passing the txt filename now
	- started simple map cache system, not implemented yet.
	
R12 6/8/09 - CTS
	- added force redraw button, for what ever reason, that's why it's "forced"
	- added draw ascii option, doesn't save with settings, might come in handy later, but as of now
		it seems to be useless, might remove it later on down the road if it is still useless needs testing first though
	- quick fixes, with when the connection art is shown, and cant remember what else, just some stuff stuff
		that I noticed after committing

R11 6/8/09 - CTS
	- ported image control and map drawing into it's own class (Thank God :D)
	- added draw asciiArt, might even try this on some maps, though I think it renders pure white
		maps, might was well give it a try
	- works still: map dragging onto program
	- added splash and connected now that asciiArt is up and runnin
	
R10 6/8/09 - CTS
	- changed how map writes to file, so first line is the width and height
	- changed the if function of the invert function to a much smaller efficient code block,
		also adds more opportunity to parse the map out different by a number rather than 1 or 0
	- changed project name and namespace to AO Map Client
	- changed SVN/Folder structure :), much cleaner solution explorer now
	- bug found out, that run w/o debug wasn't working because of reliability on map files/folder,
		so that's something to fix in the future when I work out a class structure

RV9 6/7/09 - ejact
	-added saving the dumped maps according to arealvl name

RV8 6/7/09 - CTS
	- fixed map caching problems with locations
	- changed invert into a check box
	- added saving for the invert check box
	- removed some useless clutter in map events

RV7 6/7/09 - CTS
	- changed the direct map writing to write to a file, which can be sorted out later
	- added in map reading method
	- added save perspective setting
	- changed how it checks for no map before processing perspective
	- added in "invert" which doesn't do exactly what it sounds but it produces
		a different map which may handy for some dungeon levels.
	- added splash, connect map files :P
	- added drag and drop, so you can draw map files w/o a connection

RV6 6/7/09 - CTS
	- changed connection area into a panel
	- added collapse for connection panel
	- changed buffer size for maps according to kings change of adding on an extra 0
	- fixed problem with new maps not being drawn in perspective mode if true
	- enabled unsafe code (properties, build, unsafe code), for testing out image inverting the quick way
	- SideNote: king has fixed, the buffer, and disconnect issue

RV5 6/7/09 - ejact
	- added settings module for easy accessing the ini in lokzz style ;)
	- automatic save of the setting while writing to the textboxes(minor change)
	- added all mapclient events to the event handler in the form
	- catched error while clicking the checkbox without being connected to the core
	- error clicking on connect without core being active fixed( catching socket errors)
	- cleaned up unnecessary vars
	
RV4 6/7/09 - CTS_AE
	- added map perspective
	- changed way map draws to screen used a picturebox, now I dont have to worry about painting