﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace AO_Map_Client
{
    public abstract class AoSocket : IDisposable
    {
        private string ip = "127.0.0.1";
        private int port = 2005;
        private Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public AoSocket() {}

        public AoSocket(string ip)
        {
            this.ip = ip;
        }

        public AoSocket(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }

        public bool IsClientConnected
        {
            get
            {
                try
                {
                    return client.Connected;
                }
                catch (Exception e)
                {
                    SocketErrorHandler(e.Message);
                    return false;
                }
            }
        }

        protected bool Connect()
        {
            try
            {
                client.Connect(new IPEndPoint(IPAddress.Parse(ip),port));
                return client.Connected;
            }
            catch (Exception e) { SocketErrorHandler(e.Message);  }
            return false;
        }

        protected bool StartListener()
        {
            try
            {
                if (client.Connected)
                {
                    new Thread(new ParameterizedThreadStart(this.Listener_Worker)).Start();
                    return true;
                }
            }
            catch (Exception e) { SocketErrorHandler(e.Message); }
            return false;
        }

        protected bool Disconnect()
        {
            try
            {
                client.Disconnect(true);
                return client.Connected;
            }
            catch (Exception e) { SocketErrorHandler(e.Message); }
            return false;
        }

        protected int Send(byte[] buffer, int length)
        {
            try
            {
                if (client.Connected)
                    return client.Send(buffer, length, SocketFlags.None);
            }
            catch (Exception e) { SocketErrorHandler(e.Message); }
            return 0;
        }

        public void Stop()
        {
            this.Dispose();
        }

        protected void Listener_Worker(object obj)
        {
            try
            {
                while (client.Connected)
                {
                    using (NetworkStream stream = new NetworkStream(client))
                    {
                        if (stream.DataAvailable)
                        {
                            using (BinaryReader r = new BinaryReader(stream))
                            {
                                int length = r.ReadInt32();
                                Console.WriteLine(length.ToString());
                                byte[] buffer = new byte[length];

                                if (length >= 1)
                                {
                                    buffer = r.ReadBytes(length);
                                    /*for (int i = 0; i < length; i++)
                                    {
                                        buffer[i] = r.ReadByte();
                                        
                                        Console.WriteLine(buffer[i].ToString());
                                    }*/
                                }

                                PacketHandler(buffer);
                            }
                        }
                    }
                    Thread.Sleep(100);
                }
                SocketErrorHandler("Core disconnected");
            }
            catch (Exception e) { SocketErrorHandler(e.Message); }
        }

        abstract protected void PacketHandler(byte[] packets);

        abstract protected void SocketErrorHandler(string errmsg);

        public void Dispose()
        {
            try
            {
                client.Close();
            }
            catch (Exception) { }
        }
    }
}
