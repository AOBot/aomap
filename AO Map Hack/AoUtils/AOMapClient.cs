﻿using System;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
namespace AO_Map_Client
{
    public class AOMapClient : AoSocket
    {

        private enum MapEventPackets : byte
        {
            CORECONNECTED = 0x01,
            FAILED = 0x02,
            MAPADD = 0x03,
            LEVELCHANGE = 0x04,
            PLAYERPOS = 0x05,
            TELEPATH = 0x06,
            EXIT = 0x07,
            NPC = 0x08,
            OBJECT = 0x09
        }

        public event AOMapClient_Handler MapChanged;

        public AOMapClient()
            : base()
        {
        }

        public AOMapClient(string ip)
            : base(ip)
        {
        }
 
        public int CreateNewMapClient(string ownername)
        {
            if (this.Connect())
            {
                int length = ownername.Length + 2;
                byte[] ownerbuffer = Encoding.ASCII.GetBytes(ownername);
                byte[] buffer = new byte[length];

                buffer[0] = 0x01; // tell AO to create a new map client
                Array.Copy(ownerbuffer, 0, buffer, 1, ownerbuffer.Length); // copy all bytes of the ownwername into the buffer
                buffer[length - 1] = (byte)'\0'; // end the string
                return this.Send(buffer, length);
            }
            return 0;
        }

        public bool Start()
        {
            if (IsClientConnected)
                return this.StartListener();
            return false;
        }

        protected override void PacketHandler(byte[] packets)
        {
            switch ((MapEventPackets)packets[0])
            {        
                case MapEventPackets.CORECONNECTED:
                    MapChanged(this, new MapArgs(MapArgs.Event.CORECONNECTED, "Core connection established!"));
                    break;

                case MapEventPackets.FAILED:
                    MapChanged(this, new MapArgs(MapArgs.Event.FAILED, "failed"));
                    break;

                case MapEventPackets.MAPADD:
                    MapStruct map = new MapStruct();
                    map.area = (AreaLevel) BitConverter.ToUInt32(packets, 1);
                    map.width = BitConverter.ToUInt32(packets, 5);
                    map.height = BitConverter.ToUInt32(packets, 9);
                    map.mapdata = new byte[map.width * map.height];

                    Array.Copy(packets, 13, map.mapdata, 0, map.mapdata.Length);

                    MapChanged(this, new MapArgs(map));
                    break;

                case MapEventPackets.LEVELCHANGE:
                    AreaLevel area = (AreaLevel) BitConverter.ToUInt32(packets, 1);
                    //mapname = ((AreaLevel)areaid).ToString() + ".txt";
                    MapChanged(this, new MapArgs(area));
                    break;
                    
                case MapEventPackets.PLAYERPOS:
                    //System.out.println("update position");
                    int xpos = BitConverter.ToInt32(packets, 1);
                    int ypos = BitConverter.ToInt32(packets, 5);
                    MapChanged(this, new MapArgs(new Point(xpos, ypos)));
                    break;

                case MapEventPackets.TELEPATH:

                    int size = BitConverter.ToInt32(packets, 1);

                    Point [] tpath = new Point[size];
    
                    int index = 5;

                    for (int i = 0; i < size; i++)
                    {
                         int x = BitConverter.ToInt32(packets, index);
                         index = index + 4;
                         int y = BitConverter.ToInt32(packets, index);
                         index = index + 4;
                         tpath[i] = new Point(x, y); 
                    }
                    MapChanged(this, new MapArgs(tpath));

                    break;

                case MapEventPackets.EXIT:

                    Exit exit = new Exit();
                    exit.from = (AreaLevel)BitConverter.ToInt32(packets, 1);
                    exit.to = (AreaLevel) BitConverter.ToInt32(packets, 5);
                    exit.location = new Point(BitConverter.ToInt32(packets, 9), BitConverter.ToInt32(packets, 13));
                    MapChanged(this, new MapArgs(exit));

                    //System.out.println("exit from " + from + " to " + to + " at " + x + ", " + y);
               
                    break;
                
                case MapEventPackets.NPC:
                    //break;
                    Npc npc = new Npc();
                    npc.area = (AreaLevel)BitConverter.ToInt32(packets, 1);
                    npc.npcid = BitConverter.ToInt32(packets, 5);
                    npc.location = new Point(BitConverter.ToInt32(packets, 9), BitConverter.ToInt32(packets, 13));
                    MapChanged(this, new MapArgs(npc));
                    break;

                case MapEventPackets.OBJECT:
                    //break;
                    Object obj = new Object();
                    obj.area = (AreaLevel)BitConverter.ToInt32(packets, 1);
                    obj.objectid = BitConverter.ToInt32(packets, 5);
                    obj.location = new Point(BitConverter.ToInt32(packets, 9), BitConverter.ToInt32(packets, 13));
                    MapChanged(this, new MapArgs(obj));
                    break;
            }
        }

        protected override void SocketErrorHandler(string errmsg)
        {
            MapChanged(this, new MapArgs(MapArgs.Event.SOCKETERROR, errmsg));
        }
    }

    public delegate void AOMapClient_Handler(object sender, MapArgs myArgs);

    public class MapArgs : EventArgs
    {
        private Event _event;
        private MapStruct map = null;
        private string info;
        private Point playerpos = new Point(0,0);
        private Point[] tpath = null;
        private AreaLevel area;
        private Exit exit = null;
        private Npc npc = null;
        private Object obj = null;

        #region Constructors
        public MapArgs(Event _event, string info)
        {
            this._event = _event;
            this.info = info;
        }

        public MapArgs(MapStruct map)
        {
            this._event = Event.MAPADD;
            this.map = map;
            this.area = map.area;
            this.info = "Map Add";
        }

        public MapArgs(Point playerpos)
        {
            this._event = Event.PLAYERPOS;
            this.playerpos = playerpos;
            this.info = "Player Position";
        }

        public MapArgs(AreaLevel area)
        {
            this._event = Event.LEVELCHANGE;
            this.area = area;
            this.info = "Level Change";
        }

        public MapArgs(Point[] tpath)
        {
            this._event = Event.TELEPATH;
            this.tpath = tpath;
            this.info = "Teleport Path";
        }

        public MapArgs(Exit exit)
        {
            this._event = Event.EXIT;
            this.exit = exit;
            this.info = "Exit";
        }

        public MapArgs(Npc npc)
        {
            this._event = Event.NPC;
            this.npc = npc;
            this.info = "NPC";
        }

        public MapArgs(Object obj)
        {
            this._event = Event.OBJECT;
            this.obj = obj;
            this.info = "Object";
        }
        #endregion

        #region public Eventargs
        public Event _Event
        {
            get { return this._event; }
        }

        public MapStruct Map
        {
            get { return this.map; }
        }

        public Point Playerpos
        {
            get { return this.playerpos; }
        }

        public string Info
        {
            get { return this.info; }
        }

        public AreaLevel Area
        {
            get { return this.area; }
        }

        public Point[] TPath
        {
            get { return this.tpath; }
        }

        public Exit _Exit
        {
            get { return this.exit; }
        }

        public Npc _Npc
        {
            get { return this.npc; }
        }

        public Object Obj
        {
            get { return this.obj; }
        }

        #endregion

        public enum Event
        {
            CORECONNECTED,
            FAILED,
            MAPADD,
            LEVELCHANGE,
            PLAYERPOS,
            TELEPATH ,
            EXIT ,
            NPC ,
            OBJECT ,
            SOCKETERROR
        }

    }
}
