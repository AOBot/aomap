﻿using System;
using System.Collections;
//using System.Text;
using System.Drawing;
using BitmapControl;
using System.Runtime.InteropServices;

namespace AO_Map_Client
{
    class Drawing
    {
        #region Draw Directly To Desktop
        [DllImport("User32.dll")]
        public static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("User32.dll")]
        public static extern void ReleaseDC(IntPtr dc);
        #endregion

        static public Bitmap redcross(int x, int y, int winW, int winH, bool perspective, int zoom)
        {

            Bitmap bmp = new Bitmap(winW, winH);
            Graphics gImage = Graphics.FromImage(bmp);

            // horizontal
            gImage.FillRectangle(
                Brushes.Red,
                x - Convert.ToInt32(8 * zoom), y - Convert.ToInt32(2 * zoom),
                16, 4);

            // vertical
            gImage.FillRectangle(
                Brushes.Blue,
                x - Convert.ToInt32(2 * zoom), y - Convert.ToInt32(8 * zoom),
                4, 16);
            if (perspective) return BitmapFunctions.PerspectiveD2(bmp);
            else return bmp;

            #region Draw Directly To Desktop
            /*
            IntPtr desktopDC = GetDC(IntPtr.Zero);

            Graphics gImage = Graphics.FromHdc(desktopDC);

            gImage.FillRectangle(
                Brushes.Red,
                x - Convert.ToInt32(8 * zoom), y - Convert.ToInt32(2 * zoom),
                16, 4);

            // vertical
            gImage.FillRectangle(
                Brushes.Blue,
                x - Convert.ToInt32(2 * zoom), y - Convert.ToInt32(8 * zoom),
                4, 16);


            gImage.Dispose();

            ReleaseDC(desktopDC);
            */
            #endregion
        }

        static public Bitmap orangeCrossSmall(ArrayList Exits, int winW, int winH, bool perspective, int zoom)
        {

            Bitmap bmp = new Bitmap(winW, winH);
            Graphics gImage = Graphics.FromImage(bmp);

            foreach (Exit xIt in Exits)
            {
                // horizontal
                gImage.FillRectangle(
                    Brushes.Red,
                    xIt.location.X - Convert.ToInt32(4 * zoom), xIt.location.Y - Convert.ToInt32(1 * zoom),
                    8, 2);

                // vertical
                gImage.FillRectangle(
                    Brushes.Red,
                    xIt.location.X - Convert.ToInt32(1 * zoom), xIt.location.Y - Convert.ToInt32(4 * zoom),
                    2, 8);
            }

            if (perspective) return BitmapFunctions.PerspectiveD2(bmp);
            else return bmp;
        }

        static public Bitmap WayPointBmp(ArrayList Exits, int winW, int winH, bool perspective, int zoom)
        {

            Bitmap bmp = new Bitmap(winW, winH);
            Graphics gImage = Graphics.FromImage(bmp);

            foreach (Exit xIt in Exits)
            {
                // horizontal
                gImage.FillRectangle(
                    Brushes.Red,
                    xIt.location.X - Convert.ToInt32(4 * zoom), xIt.location.Y - Convert.ToInt32(1 * zoom),
                    8, 2);

                // vertical
                gImage.FillRectangle(
                    Brushes.Red,
                    xIt.location.X - Convert.ToInt32(1 * zoom), xIt.location.Y - Convert.ToInt32(4 * zoom),
                    2, 8);

                // bluebox bottom left
                gImage.FillRectangle(
                    Brushes.Blue,
                    xIt.location.X - Convert.ToInt32(4 * zoom), xIt.location.Y + Convert.ToInt32(2 * zoom),
                    2, 2);

                // bluebox top right
                gImage.FillRectangle(
                    Brushes.Blue,
                    xIt.location.X + Convert.ToInt32(2 * zoom), xIt.location.Y - Convert.ToInt32(4 * zoom),
                    2, 2);
            }

            if (perspective) return BitmapFunctions.PerspectiveD2(bmp);
            else return bmp;
        }

        static public Bitmap pointBox(string name, int x, int y, int width, int height, Color color, bool perspective, int zoom)
        {

            Bitmap bmp = new Bitmap(width, height);
            Graphics gImage = Graphics.FromImage(bmp);
            Brush brush = new SolidBrush(color);

            gImage.FillRectangle(
                brush,
                x, y,
                zoom, zoom);
            gImage.DrawString(name, new Font("Trebuchet MS", 8), brush, new PointF(x, y-9));

            if (perspective) return BitmapFunctions.PerspectiveD2(bmp);
            else return bmp;
        }

        static public Bitmap telepath()
        {
            return new Bitmap(0,0);
        }

    }
}
