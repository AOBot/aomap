﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using AO_Map_Client;

namespace BitmapControl
{
    public class BitmapFunctions
    {
        public static void SetQuality(Graphics gfx)
        {
            if (Settings.PrecisionDrawing)
            {
                gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            }
            else
            {
                //gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Default;
                //gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
                gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            }
        }

        public static Size Rotate45ReturnSize(Size size)
        {
            int thesquare = Convert.ToInt32((size.Width * Math.Sqrt(2) / 2) + (size.Height * Math.Sqrt(2) / 2));
            return new Size(thesquare, thesquare);
        }

        public static Bitmap RotateImage(Bitmap b, float angle)
        {
            Bitmap returnBitmap = new Bitmap(Rotate45ReturnSize(b.Size).Width, Rotate45ReturnSize(b.Size).Height);
            Graphics g = Graphics.FromImage(returnBitmap);
            SetQuality(g);
            g.RotateTransform(angle);
            g.DrawImage(b, b.Height / 2, -b.Height / 2); //b.Width / 2, -b.Width / 2);//
            return returnBitmap;
        }

        public static Bitmap ResizeBitmap(Bitmap b, int nWidth, int nHeight)
        {
            Bitmap result = new Bitmap(nWidth, nHeight);
            using (Graphics g = Graphics.FromImage(result))
            {
                SetQuality(g);
                g.DrawImage(b, 0, 0, nWidth, nHeight);
            }
            return result;
        }

        // applies 45 degree rotation and then 50% height
        public static Bitmap PerspectiveD2(Bitmap b)
        {
            Bitmap result = new Bitmap(Rotate45ReturnSize(b.Size).Width, Rotate45ReturnSize(b.Size).Height);
            Graphics g = Graphics.FromImage(result);
            SetQuality(g);
            result = RotateImage(b, 45);
            result = ResizeBitmap(result, result.Width, result.Height / 2);
            return result;
        }

        public static Bitmap InvertImageUnsafe(Bitmap originalImg)
        {
            Bitmap bmp = new Bitmap(originalImg);

            //Format is BGRA, NOT ARGB.
            BitmapData bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                                           ImageLockMode.ReadWrite,
                                           PixelFormat.Format32bppArgb);
            int stride = bmData.Stride;
            IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bmp.Width * 4;
                int nWidth = bmp.Width;

                for (int y = 0; y < bmp.Height; y++)
                {
                    for (int x = 0; x < nWidth; x++)
                    {
                        p[0] = (byte)(255 - p[0]); //red
                        p[1] = (byte)(255 - p[1]); //green
                        p[2] = (byte)(255 - p[2]); //blue
                        //p[3] is alpha, don't want to invert alpha

                        p += 4;
                    }
                    p += nOffset;
                }
            }
            bmp.UnlockBits(bmData);
            return bmp;
        }

        public static Bitmap Dither(Image originalImg)
        {
            Bitmap bmp = new Bitmap(originalImg);

            //Format is BGRA, NOT ARGB.
            BitmapData bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                                           ImageLockMode.ReadWrite,
                                           PixelFormat.Format32bppArgb);
            int stride = bmData.Stride;
            IntPtr Scan0 = bmData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bmp.Width * 4;
                int nWidth = bmp.Width;

                for (int y = 0; y < bmp.Height; y++)
                {
                    for (int x = 0; x < nWidth; x = x + 2)
                    {
                        //p[0] = (byte)(255 - p[0]); //red
                        //p[1] = (byte)(255 - p[1]); //green
                        //p[2] = (byte)(255 - p[2]); //blue
                        p[3] = (byte)(0);// if we dont put @ 0 we can recover the data on the pixel
                        // but for our sake... the window wont be able to see through that pixel with
                        // the color keying from the window
                        // maybe use fancy png gui dealio? : \

                        p += 8;//4;//
                    }
                    p += nOffset;
                }

            }
            bmp.UnlockBits(bmData);
            return bmp;
        }


    }
}
