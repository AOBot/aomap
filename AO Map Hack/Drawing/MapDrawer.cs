﻿using System;
using System.Text;
using System.Drawing;
using System.IO;
using System.Threading;
using BitmapControl;

namespace AO_Map_Client
{
    public delegate void MapDrawer_Handler(object sender, Bitmap mapbmp);

    class MapDrawer
    {
        private string ApplicationPath;
        private int windowWidth;
        private int windowHeight;
        private MapStruct map;
        private bool perspective;
        private bool dither;
        private bool invert;
        private bool multiTone;
        private double zoom;

        public event MapDrawer_Handler MapDrawn;

        public MapDrawer(string ApplicationPath)
        {
            this.ApplicationPath = ApplicationPath;
        }

        public void Draw(MapStruct map, int windowWidth, int windowHeight, bool perspective, bool dither, bool invert, bool multiTone, double zoom)
        {
            this.map = map;
            this.windowWidth = windowWidth;
            this.windowHeight = windowHeight;
            this.perspective = perspective;
            this.dither = dither;
            this.invert = invert;
            this.multiTone = multiTone;
            this.zoom = zoom;

            if (map != null) new Thread(new ParameterizedThreadStart(this.DrawMap)).Start();
        }

        public Bitmap DrawAcsiiArt(string asciiArtLocation, int WindowWidth, int WindowHeight)
        {
            if (windowHeight < 0 || windowWidth < 0)
                return new Bitmap(0, 0);

            StreamReader SR;
            SolidBrush myBrush = new SolidBrush(Color.White);
            int x = 0;
            int y = 0;
            int longestLine = 0;
            int lineAmount = 0;
            string S;
            windowWidth = WindowWidth - 15; // for padding
            windowHeight = WindowHeight - 90; // for padding and lower pannel

            // finds longest line in document
            SR = File.OpenText(asciiArtLocation.Contains(":") == false ?
                ApplicationPath + asciiArtLocation : 
                asciiArtLocation);
            S = SR.ReadLine();
            while (S != null)
            {
                if (S.Length > longestLine) longestLine = S.Length;
                lineAmount++;
                S = SR.ReadLine();
            }
            SR.Close();

            Bitmap result = new Bitmap(windowWidth, windowHeight);
            Graphics g = Graphics.FromImage(result);
            int xscale = windowWidth / (longestLine + 4); // the adding is for padding
            int yscale = windowHeight / (lineAmount + 4);

            SR = File.OpenText(asciiArtLocation.Contains(":") == false ?
                ApplicationPath + asciiArtLocation :
                asciiArtLocation);
            S = SR.ReadLine();
            while (S != null)
            {
                foreach (char i in S)
                {
                    if (Convert.ToInt32(i) != 32) //32 = blank space
                    {
                        g.FillRectangle(
                            Brushes.White, x, y,
                            Convert.ToInt32(1 * xscale), Convert.ToInt32(1 * yscale));
                    }
                    x = Convert.ToInt32(x + 1 * xscale);
                }
                x = 0;
                y = Convert.ToInt32(y + 1 * yscale);
                S = SR.ReadLine();
            }
            SR.Close();

            return result;
            //MapDrawn(this, result);
        }

        private void DrawMap(object obj)
        {
            if (multiTone)
            {
                MultiToneMap();
                return;
            }

            //Bitmap bmp = new Bitmap(windowWidth, windowHeight);
            Bitmap bmp = new Bitmap((int)map.width, (int)map.height);
            Graphics gImage = Graphics.FromImage(bmp);
            BitmapControl.BitmapFunctions.SetQuality(gImage);

            int pos = 0 ;

            for (int y = 0; y < map.height; y++)
            {
                        
                for (int x = 0; x < map.width; x++ )
                {
                    if (map.mapdata[pos++].Equals((byte)Convert.ToInt32(invert)))
                    {
                        gImage.FillRectangle(
                            Brushes.White, x, y,
                            Convert.ToInt32(1 * zoom), Convert.ToInt32(1 * zoom));
                    }
                }
                         
            }

            //////TEST
            gImage.DrawRectangle(new Pen(Brushes.Red), 0, 0, (int)map.width, (int)map.height);

            if (perspective)
                bmp = BitmapFunctions.PerspectiveD2(bmp);
            if (dither)
                bmp = BitmapFunctions.Dither(bmp);

            MapDrawn(this, bmp);
        }

        private void MultiToneMap()
        {
            Bitmap bmp = new Bitmap((int)map.width, (int)map.height);
            Graphics gImage = Graphics.FromImage(bmp);

            int pos = 0;

            try
            {
                for (int y = 0; y < map.height; y++)
                {

                    for (int x = 0; x < map.width; x++)
                    {
                        if (map.mapdata[pos].Equals((byte)Convert.ToInt32(invert == false ? true : false)))
                        {
                            gImage.FillRectangle(
                                Brushes.White, x, y,
                                Convert.ToInt32(1 * zoom), Convert.ToInt32(1 * zoom));
                        }

                        else if (map.mapdata[pos].Equals((byte)Convert.ToInt32(invert)))
                        {
                            gImage.FillRectangle(
                               Brushes.Silver, x, y,
                               Convert.ToInt32(1 * zoom), Convert.ToInt32(1 * zoom));
                        }
                        pos++;
                    }
                    // Very Interesting : \
                    #region interesting
                    /*                      
                for (int y = 0; y < map.height; y=y+2)
                {

                    for (int x = 0; x < map.width; x=x+2)
                    {
                        if (map.mapdata[pos].Equals((byte)Convert.ToInt32(invert == false ? true : false)))
                        {
                            gImage.FillRectangle(
                                Brushes.White, x*2, y*2,
                                Convert.ToInt32(1 * zoom), Convert.ToInt32(1 * zoom));
                        }

                        else if (map.mapdata[pos].Equals((byte)Convert.ToInt32(invert)))
                        {
                            gImage.FillRectangle(
                               Brushes.Silver, x, y,
                               Convert.ToInt32(1 * zoom), Convert.ToInt32(1 * zoom));
                        }
                        pos=pos+2;
                    }

                }
                */
                    #endregion
                }
            }
            catch { }

            if (perspective)
                bmp = BitmapFunctions.PerspectiveD2(bmp);
            if (dither)
                bmp = BitmapFunctions.Dither(bmp);

            MapDrawn(this, bmp);

        }

    }
}

