﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Windows.Forms;

namespace AO_Map_Client
{

    /// <summary>
    /// Provides an interface to the different cache types as well as
    /// batch commands to all of the caches
    /// </summary>
    [Serializable]
    public class Caching
    {

        public MapCache _MapCache;
        public ExitCache _ExitCache;

        // constructor
        public Caching()
        {
            _MapCache = new MapCache();
            _ExitCache = new ExitCache();
        }

        public void Save()
        {
            _MapCache.Save();
            _ExitCache.Save();
        }

        public void Load()
        {
            _MapCache = MapCache.Load();
            _ExitCache = ExitCache.Load();
        }

        public void ClearCache()
        {
            _MapCache.ClearCache();
            _ExitCache.ClearCache();
        }

        /// <summary>
        /// A data collection of MapStructs / map info
        /// Can be saved into an external binary file for recalling the object
        /// </summary>
        [Serializable]
        public class MapCache
        {
            private Dictionary<AreaLevel, MapStruct> cachedmaps = new Dictionary<AreaLevel, MapStruct>();

            public MapCache() { }

            public bool AddMapToCache(AreaLevel area, MapStruct map)
            {
                try
                {
                    cachedmaps.Add(area, map);
                    return true;
                }
                catch (ArgumentException)
                {
                    //Key already exists:
                    return false;
                }
            }

            public MapStruct GetMapFromCache(AreaLevel area)
            {
                try
                {
                    if (cachedmaps.ContainsKey(area))
                    {
                        MapStruct map;
                        cachedmaps.TryGetValue(area, out map);
                        return map;
                    }
                }
                catch
                {
                    MessageBox.Show("GET MAP FROM CACHE MAPS");
                }
                return null;
            }

            public bool IsMapInCache(AreaLevel area)
            {
                return cachedmaps.ContainsKey(area);
            }

            public void ClearCache()
            {
                cachedmaps.Clear();
                Save();
            }

            public int Count
            {
                get 
                {
                    try { return cachedmaps.Count; }
                    catch { return 0; }
                }
            }

            public void Save()
            {
                try
                {
                    File.Delete(Application.CommonAppDataPath + "\\AOMap Map.cbin");
                    Stream s = File.OpenWrite(Application.CommonAppDataPath + "\\AOMap Map.cbin");
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(s, this);
                    s.Close();
                }
                catch { }
            }

            public static MapCache Load()
            {
                try
                {
                    FileStream file = new FileStream(Application.CommonAppDataPath + "\\AOMap Map.cbin",
                        FileMode.Open);
                    BinaryFormatter bf = new BinaryFormatter();
                    return bf.Deserialize(file) as MapCache;
                }
                catch { return new MapCache(); }
            }

        }

        /// <summary>
        /// A data collection of Exit Structs
        /// Can be saved into an external binary file for recalling the object
        /// </summary>
        [Serializable]
        public class ExitCache
        {
            ArrayList cachedexits = new ArrayList();

            public ExitCache() { }

            public bool AddExitToCache(Exit exit)
            {
                try
                {
                    cachedexits.Add(exit);
                    return true;
                }
                catch (ArgumentException)
                {
                    //Key already exists:
                    return false;
                }
            }

            public ArrayList GetExitsFromCache(AreaLevel AreaFrom)
            {
                try
                {
                    ArrayList foundExits = new ArrayList();

                    foreach (Exit x in cachedexits)
                    {
                        if (x.from == AreaFrom)
                            foundExits.Add(x);
                    }

                    if (foundExits.Count != 0)
                        return foundExits;
                    else
                        return null;
                }
                catch
                {
                    MessageBox.Show("GET MAP FROM CACHE MAPS");
                }
                return null;
            }

            public void ClearCache()
            {
                cachedexits.Clear();
            }

            public int Count
            {
                get 
                {
                    try { return cachedexits.Count; }
                    catch { return 0; }
                }
            }

            public void Save()
            {
                try
                {
                    File.Delete(Application.CommonAppDataPath + "\\AOMap Exit.cbin");
                    Stream s = File.OpenWrite(Application.CommonAppDataPath + "\\AOMap Exit.cbin");
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(s, this);
                    s.Close();
                }
                catch { }
            }

            public static ExitCache Load()
            {
                try
                {

                    FileStream file = new FileStream(Application.CommonAppDataPath + "\\AOMap Exit.cbin",
                        FileMode.Open);
                    BinaryFormatter bf = new BinaryFormatter();
                    return bf.Deserialize(file) as ExitCache;
                }
                catch { return new ExitCache(); }
            }

        }
    }

}
