﻿using System;
using System.Threading;
using System.Diagnostics;

namespace AO_Map_Client
{
    class FollowWindow
    {
        Form_Main Parent;
        int ID;
        public Thread Follow;
        public bool Following;
        public bool QuickStop;

        public delegate void WindowStateChange(object sender, System.Windows.Forms.FormWindowState formWindowState);
        public event WindowStateChange WinStateChange;

        public FollowWindow(Form_Main Parent, int ID)
        {
            this.Parent = Parent;
            this.ID = ID;
        }

        public void Follower()
        {
            while (true)
            {
                try
                {
                    if (Process.GetProcessById(ID).HasExited == false) // this will throw and prohibit a 0,0 move
                    {
                        if (Settings.Grab_D2_Follows_Map_Client) // maybe bad to keep on calling the ini value?
                            WinControl.winSetPosition(ID, Parent.Location.X, Parent.Location.Y);
                        else
                            WinControl.winSetPosition(System.Diagnostics.Process.GetCurrentProcess().Id, WinControl.winGetPosition(ID).X, WinControl.winGetPosition(ID).Y);

                        // detect d2 window state change, and then send event to main form to follow the leader
                        if (WinControl.GetWindowState(ID) == WinControl.WindowShowStyle.ShowMinimized)
                            this.WinStateChange(this, System.Windows.Forms.FormWindowState.Minimized);
                        else if (WinControl.GetWindowState(ID) == WinControl.WindowShowStyle.ShowNormal)
                            this.WinStateChange(this, System.Windows.Forms.FormWindowState.Normal);
                        else if (WinControl.GetWindowState(ID) == WinControl.WindowShowStyle.Maximize)
                            this.WinStateChange(this, System.Windows.Forms.FormWindowState.Maximized);
                    }
                    WinControl.winSetText(ID, "Diablo II     ID: " + ID);
                }
                catch (Exception e)
                {
                    Toggle(false);
                    Parent.Debug("Error in the following thread" + e.Message, false);
                }

                Thread.Sleep(333);
            }
        }

        void Parent_WinStateChange(object sender, System.Windows.Forms.FormWindowState formWindowState)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            if (Follow == null)
            {
                Follow = new Thread(new ThreadStart(Follower));
                Follow.Start();
            }
            else if (Follow.ThreadState == System.Threading.ThreadState.Stopped)
            {
                Follow = new Thread(new ThreadStart(Follower));
                Follow.Start();
            }
                
            Following = true;
            QuickStop = false;
        }

        public void Toggle(bool On)
        {
            if (On)
            {
                this.Start();
            }
            else
            {
                Follow.Abort();
                Parent.Debug("Stopped Following D2 Window: " + ID, true);
                Following = false;
            }
        }

    }
}
