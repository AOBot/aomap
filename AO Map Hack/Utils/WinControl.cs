﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace AO_Map_Client
{
    class WinControl
    {
        public WinControl()
        {

        }


        #region Handle and ID

        public static IntPtr ID2Handle(int ID)
        {
            try
            {
                return Process.GetProcessById(ID).MainWindowHandle;
            }
            catch { return IntPtr.Zero; }
        }

        public static int GetIDTitle(string Title)
        {
                foreach (Process p in Process.GetProcesses())
                {
                    if (p.MainWindowTitle == Title) return p.Id;
                }
                return 0;
        }

        public static int GetIDName(string Name)
        {
            foreach (Process p in Process.GetProcesses())
            {
                if (p.ProcessName == Name) return p.Id;
            }
            return 0;
        }

        #endregion

        #region Window Gets

        [DllImport("user32.dll")]
        public static extern long GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hwnd, StringBuilder buf, int nMaxCount);

        [DllImport("user32.dll", SetLastError=true, CharSet=CharSet.Auto)]
        static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);
        public struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public int showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;
        }

        public static Point winGetPosition(int ID)
        {
            WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
            placement.length = Marshal.SizeOf(placement);
            GetWindowPlacement(ID2Handle(ID), ref placement);
            return new Point(placement.rcNormalPosition.X, placement.rcNormalPosition.Y);
        }

        public static Size winGetSize(int ID)
        {
            WINDOWPLACEMENT wp = new WINDOWPLACEMENT();
            GetWindowPlacement(ID2Handle(ID), ref wp);
            Point p = winGetPosition(ID);
            return new Size(wp.rcNormalPosition.Width - p.X , wp.rcNormalPosition.Height - p.Y);
        }

        public static WindowShowStyle GetWindowState(int ID)
        {
            WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
            placement.length = Marshal.SizeOf(placement);
            GetWindowPlacement(ID2Handle(ID), ref placement);
            return (WindowShowStyle)placement.showCmd;
        }

        public static string winGetText(int ID)
        {
        // Allocate correct string length first
        int length = GetWindowTextLength(ID2Handle(ID));
        StringBuilder sb = new StringBuilder(length + 1);
        GetWindowText(ID2Handle(ID), sb, sb.Capacity);
        return sb.ToString();
        }

        #endregion

        #region Window Sets

        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, long dwNewLong);

        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int posX,
            int posY, int width, int height, uint uFlags);

        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOP = new IntPtr(0);

        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 SWP_NOZORDER = 0x0004;
        const UInt32 SWP_NOREDRAW = 0x0008;
        const UInt32 SWP_NOACTIVATE = 0x0010;
        const UInt32 SWP_FRAMECHANGED = 0x0020;  /* The frame changed: send WM_NCCALCSIZE */
        const UInt32 SWP_SHOWWINDOW = 0x0040;
        const UInt32 SWP_HIDEWINDOW = 0x0080;
        const UInt32 SWP_NOCOPYBITS = 0x0100;
        const UInt32 SWP_NOOWNERZORDER = 0x0200;  /* Don't do owner Z ordering */
        const UInt32 SWP_NOSENDCHANGING = 0x0400;  /* Don't send WM_WINDOWPOSCHANGING */

        const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        public static void winSetNoSize(int ID)
        {
            SetWindowPos(ID2Handle(ID), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE);
        }

        public static void winSetNoMove(int ID)
        {
            SetWindowPos(ID2Handle(ID), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE);
        }

        public static void winSetPosition(int ID, int x, int y)
        {
            SetWindowPos(ID2Handle(ID), HWND_NOTOPMOST, x, y, 0, 0, SWP_NOSIZE);
        }

        public static void winSetSize(int ID, int width, int height)
        {
            SetWindowPos(ID2Handle(ID), HWND_NOTOPMOST, 0, 0, width, height, SWP_NOMOVE);
        }

        public static void winSetShow(int ID)
        {
            SetWindowPos(ID2Handle(ID), HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
        }

        public static void winSetShowV2(int ID)
        {
            SetWindowPos(ID2Handle(ID), HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW | SWP_NOMOVE);
        }

        public static void winSetTopMost(int ID)
        {
            SetWindowPos(ID2Handle(ID), HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
        }

        public static void winSetNormal(int ID)
        {
            SetWindowPos(ID2Handle(ID), HWND_NOTOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
        }

        public static void winSetHide(int ID)
        {
            SetWindowPos(ID2Handle(ID), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_HIDEWINDOW);
        }

        [DllImport("User32.Dll")]
        static extern void SetWindowText(IntPtr handle, String s);

        public static void winSetText(int ID, string S)
        {
            SetWindowText(ID2Handle(ID), S);
        }

        /// <summary>
        /// Sets windows mode to Layer and All mouse interaction  is ignored
        ///  and is recieved by lower windows
        /// </summary>
        /// <param name="hwnd">Window Handle Name</param>
        public static void SetWindowLayeredMode(IntPtr hwnd)
        {
            // make window a windows overlay layer ( no clicks or mouse will be captured, and will fall to lower windows)
            SetWindowLong(hwnd, -20, GetWindowLong(hwnd, -20) | 0x00000020);
        }

        #endregion

        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, WindowShowStyle nCmdShow);

        public enum WindowShowStyle : uint
        {
            /// <summary>Hides the window and activates another window.</summary>
            /// <remarks>See SW_HIDE</remarks>
            Hide = 0,
            /// <summary>Activates and displays a window. If the window is minimized
            /// or maximized, the system restores it to its original size and
            /// position. An application should specify this flag when displaying
            /// the window for the first time.</summary>
            /// <remarks>See SW_SHOWNORMAL</remarks>
            ShowNormal = 1,
            /// <summary>Activates the window and displays it as a minimized window.</summary>
            /// <remarks>See SW_SHOWMINIMIZED</remarks>
            ShowMinimized = 2,
            /// <summary>Activates the window and displays it as a maximized window.</summary>
            /// <remarks>See SW_SHOWMAXIMIZED</remarks>
            ShowMaximized = 3,
            /// <summary>Maximizes the specified window.</summary>
            /// <remarks>See SW_MAXIMIZE</remarks>
            Maximize = 3,
            /// <summary>Displays a window in its most recent size and position.
            /// This value is similar to "ShowNormal", except the window is not
            /// actived.</summary>
            /// <remarks>See SW_SHOWNOACTIVATE</remarks>
            ShowNormalNoActivate = 4,
            /// <summary>Activates the window and displays it in its current size
            /// and position.</summary>
            /// <remarks>See SW_SHOW</remarks>
            Show = 5,
            /// <summary>Minimizes the specified window and activates the next
            /// top-level window in the Z order.</summary>
            /// <remarks>See SW_MINIMIZE</remarks>
            Minimize = 6,
            /// <summary>Displays the window as a minimized window. This value is
            /// similar to "ShowMinimized", except the window is not activated.</summary>
            /// <remarks>See SW_SHOWMINNOACTIVE</remarks>
            ShowMinNoActivate = 7,
            /// <summary>Displays the window in its current size and position. This
            /// value is similar to "Show", except the window is not activated.</summary>
            /// <remarks>See SW_SHOWNA</remarks>
            ShowNoActivate = 8,
            /// <summary>Activates and displays the window. If the window is
            /// minimized or maximized, the system restores it to its original size
            /// and position. An application should specify this flag when restoring
            /// a minimized window.</summary>
            /// <remarks>See SW_RESTORE</remarks>
            Restore = 9,
            /// <summary>Sets the show state based on the SW_ value specified in the
            /// STARTUPINFO structure passed to the CreateProcess function by the
            /// program that started the application.</summary>
            /// <remarks>See SW_SHOWDEFAULT</remarks>
            ShowDefault = 10,
            /// <summary>Windows 2000/XP: Minimizes a window, even if the thread
            /// that owns the window is hung. This flag should only be used when
            /// minimizing windows from a different thread.</summary>
            /// <remarks>See SW_FORCEMINIMIZE</remarks>
            ForceMinimized = 11
        }

        /* OBSOLETE FOR NOW
        #region Mouse Junk?
        //overloads for various actions
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        private static extern IntPtr SendMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        private static extern IntPtr SendMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, StringBuilder lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        private static extern IntPtr SendMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, String lParam);

        public static IntPtr MakeLParam(int LoWord, int HiWord)
        {
            return (IntPtr)((HiWord << 16) | (LoWord & 0xffff));
        }

        protected class WMmessages
        {
            public const int WM_NULL = 0x000;
            public const int WM_CREATE = 0x001;
            public const int WM_DESTROY = 0x002;
            public const int WM_MOVE = 0x003;
            public const int WM_SIZE = 0x005;
            public const int WM_ACTIVATE = 0x006;
            public const int WM_SETFOCUS = 0x007;
            public const int WM_KILLFOCUS = 0x008;
            public const int WM_ENABLE = 0x00A;
            public const int WM_SETREDRAW = 0x00B;
            public const int WM_SETTEXT = 0x00C;
            public const int WM_GETTEXT = 0x00D;
            public const int WM_GETTEXTLENGTH = 0x00E;
            public const int WM_PAINT = 0x00F;
            public const int WM_CLOSE = 0x010;
            public const int WM_QUERYENDSESSION = 0x011;
            public const int WM_QUIT = 0x012;
            public const int WM_QUERYOPEN = 0x013;
            public const int WM_ERASEBKGND = 0x014;
            public const int WM_SYSCOLORCHANGE = 0x015;
            public const int WM_ENDSESSION = 0x016;
            public const int WM_SHOWWINDOW = 0x018;
            public const int WM_WININICHANGE = 0x01A;
            public const int WM_DEVMODECHANGE = 0x01B;
            public const int WM_ACTIVATEAPP = 0x01C;
            public const int WM_FONTCHANGE = 0x01D;
            public const int WM_TIMECHANGE = 0x01E;
            public const int WM_CANCELMODE = 0x01F;
            public const int WM_SETCURSOR = 0x020;
            public const int WM_MOUSEACTIVATE = 0x021;
            public const int WM_CHILDACTIVATE = 0x022;
            public const int WM_QUEUESYNC = 0x023;
            public const int WM_GETMINMAXINFO = 0x024;
            public const int WM_PAINTICON = 0x026;
            public const int WM_ICONERASEBKGND = 0x027;
            public const int WM_NEXTDLGCTL = 0x028;
            public const int WM_SPOOLERSTATUS = 0x02A;
            public const int WM_DRAWITEM = 0x02B;
            public const int WM_MEASUREITEM = 0x02C;
            public const int WM_DELETEITEM = 0x02D;
            public const int WM_VKEYTOITEM = 0x02E;
            public const int WM_CHARTOITEM = 0x02F;
            public const int WM_SETFONT = 0x030;
            public const int WM_GETFONT = 0x031;
            public const int WM_SETHOTKEY = 0x032;
            public const int WM_GETHOTKEY = 0x033;
            public const int WM_QUERYDRAGICON = 0x037;
            public const int WM_COMPAREITEM = 0x039;
            public const int WM_COMPACTING = 0x041;
            public const int WM_COMMNOTIFY = 0x044; // no longer suported 
            public const int WM_WINDOWPOSCHANGING = 0x046;
            public const int WM_WINDOWPOSCHANGED = 0x047;
            public const int WM_POWER = 0x048;
            public const int WM_COPYDATA = 0x04A;
            public const int WM_CANCELJOURNAL = 0x04B;
            public const int WM_USER = 0x400;
            public const int WM_NOTIFY = 0x04E;
            public const int WM_INPUTLANGCHANGEREQUEST = 0x050;
            public const int WM_INPUTLANGCHANGE = 0x051;
            public const int WM_TCARD = 0x052;
            public const int WM_HELP = 0x053;
            public const int WM_USERCHANGED = 0x054;
            public const int WM_NOTIFYFORMAT = 0x055;
            public const int WM_CONTEXTMENU = 0x07B;
            public const int WM_STYLECHANGING = 0x07C;
            public const int WM_STYLECHANGED = 0x07D;
            public const int WM_DISPLAYCHANGE = 0x07E;
            public const int WM_GETICON = 0x07F;
            public const int WM_SETICON = 0x080;
            public const int WM_NCCREATE = 0x081;
            public const int WM_NCDESTROY = 0x082;
            public const int WM_NCCALCSIZE = 0x083;
            public const int WM_NCHITTEST = 0x084;
            public const int WM_NCPAINT = 0x085;
            public const int WM_NCACTIVATE = 0x086;
            public const int WM_GETDLGCODE = 0x087;
            public const int WM_SYNCPAINT = 0x088;
            public const int WM_NCMOUSEMOVE = 0x0A0;
            public const int WM_NCLBUTTONDOWN = 0x0A1;
            public const int WM_NCLBUTTONUP = 0x0A2;
            public const int WM_NCLBUTTONDBLCLK = 0x0A3;
            public const int WM_NCRBUTTONDOWN = 0x0A4;
            public const int WM_NCRBUTTONUP = 0x0A5;
            public const int WM_NCRBUTTONDBLCLK = 0x0A6;
            public const int WM_NCMBUTTONDOWN = 0x0A7;
            public const int WM_NCMBUTTONUP = 0x0A8;
            public const int WM_NCMBUTTONDBLCLK = 0x0A9;
            public const int WM_NCXBUTTONDOWN = 0x0AB;
            public const int WM_NCXBUTTONUP = 0x0AC;
            public const int WM_NCXBUTTONDBLCLK = 0x0AD;
            public const int WM_INPUT = 0x0FF;
            public const int WM_KEYFIRST = 0x100;
            public const int WM_KEYDOWN = 0x100;
            public const int WM_KEYUP = 0x101;
            public const int WM_CHAR = 0x102;
            public const int WM_DEADCHAR = 0x103;
            public const int WM_SYSKEYDOWN = 0x104;
            public const int WM_SYSKEYUP = 0x105;
            public const int WM_SYSCHAR = 0x106;
            public const int WM_SYSDEADCHAR = 0x107;
            public const int WM_UNICHAR = 0x109;
            public const int WM_KEYLAST = 0x109;
            public const int WM_IME_STARTCOMPOSITION = 0x10D;
            public const int WM_IME_ENDCOMPOSITION = 0x10E;
            public const int WM_IME_COMPOSITION = 0x10F;
            public const int WM_IME_KEYLAST = 0x10F;
            public const int WM_INITDIALOG = 0x110;
            public const int WM_COMMAND = 0x111;
            public const int WM_SYSCOMMAND = 0x112;
            public const int WM_TIMER = 0x113;
            public const int WM_HSCROLL = 0x114;
            public const int WM_VSCROLL = 0x115;
            public const int WM_INITMENU = 0x116;
            public const int WM_INITMENUPOPUP = 0x117;
            public const int WM_MENUSELECT = 0x11F;
            public const int WM_MENUCHAR = 0x120;
            public const int WM_ENTERIDLE = 0x121;
            public const int WM_MENURBUTTONUP = 0x122;
            public const int WM_MENUDRAG = 0x123;
            public const int WM_MENUGETOBJECT = 0x124;
            public const int WM_UNINITMENUPOPUP = 0x125;
            public const int WM_MENUCOMMAND = 0x126;
            public const int WM_CHANGEUISTATE = 0x127;
            public const int WM_UPDATEUISTATE = 0x128;
            public const int WM_QUERYUISTATE = 0x129;
            public const int WM_CTLCOLORMSGBOX = 0x132;
            public const int WM_CTLCOLOREDIT = 0x133;
            public const int WM_CTLCOLORLISTBOX = 0x134;
            public const int WM_CTLCOLORBTN = 0x135;
            public const int WM_CTLCOLORDLG = 0x136;
            public const int WM_CTLCOLORSCROLLBAR = 0x137;
            public const int WM_CTLCOLORSTATIC = 0x138;
            public const int MN_GETHMENU = 0x1E1;
            public const int WM_MOUSEFIRST = 0x200;
            public const int WM_MOUSEMOVE = 0x200;
            public const int WM_LBUTTONDOWN = 0x201;
            public const int WM_LBUTTONUP = 0x202;
            public const int WM_LBUTTONDBLCLK = 0x203;
            public const int WM_RBUTTONDOWN = 0x204;
            public const int WM_RBUTTONUP = 0x205;
            public const int WM_RBUTTONDBLCLK = 0x206;
            public const int WM_MBUTTONDOWN = 0x207;
            public const int WM_MBUTTONUP = 0x208;
            public const int WM_MBUTTONDBLCLK = 0x209;
            public const int WM_MOUSEWHEEL = 0x20A;
            public const int WM_XBUTTONDOWN = 0x20B;
            public const int WM_XBUTTONUP = 0x20C;
            public const int WM_XBUTTONDBLCLK = 0x20D;
            public const int WM_MOUSELAST = 0x20A;
            public const int WM_PARENTNOTIFY = 0x210;
            public const int WM_ENTERMENULOOP = 0x211;
            public const int WM_EXITMENULOOP = 0x212;
            public const int WM_NEXTMENU = 0x213;
            public const int WM_SIZING = 0x214;
            public const int WM_CAPTURECHANGED = 0x215;
            public const int WM_MOVING = 0x216;
            public const int WM_POWERBROADCAST = 0x218;
            public const int WM_DEVICECHANGE = 0x219;
            public const int WM_MDICREATE = 0x220;
            public const int WM_MDIDESTROY = 0x221;
            public const int WM_MDIACTIVATE = 0x222;
            public const int WM_MDIRESTORE = 0x223;
            public const int WM_MDINEXT = 0x224;
            public const int WM_MDIMAXIMIZE = 0x225;
            public const int WM_MDITILE = 0x226;
            public const int WM_MDICASCADE = 0x227;
            public const int WM_MDIICONARRANGE = 0x228;
            public const int WM_MDIGETACTIVE = 0x229;
            public const int WM_MDISETMENU = 0x230;
            public const int WM_ENTERSIZEMOVE = 0x231;
            public const int WM_EXITSIZEMOVE = 0x232;
            public const int WM_DROPFILES = 0x233;
            public const int WM_MDIREFRESHMENU = 0x234;
            public const int WM_IME_SETCONTEXT = 0x281;
            public const int WM_IME_NOTIFY = 0x282;
            public const int WM_IME_CONTROL = 0x283;
            public const int WM_IME_COMPOSITIONFULL = 0x284;
            public const int WM_IME_SELECT = 0x285;
            public const int WM_IME_CHAR = 0x286;
            public const int WM_IME_REQUEST = 0x288;
            public const int WM_IME_KEYDOWN = 0x290;
            public const int WM_IME_KEYUP = 0x291;
            public const int WM_MOUSEHOVER = 0x2A1;
            public const int WM_MOUSELEAVE = 0x2A3;
            public const int WM_NCMOUSEHOVER = 0x2A0;
            public const int WM_NCMOUSELEAVE = 0x2A2;
            public const int WM_WTSSESSION_CHANGE = 0x2B1;
            public const int WM_TABLET_FIRST = 0x2c0;
            public const int WM_TABLET_LAST = 0x2df;
            public const int WM_CUT = 0x300;
            public const int WM_COPY = 0x301;
            public const int WM_PASTE = 0x302;
            public const int WM_CLEAR = 0x303;
            public const int WM_UNDO = 0x304;
            public const int WM_RENDERFORMAT = 0x305;
            public const int WM_RENDERALLFORMATS = 0x306;
            public const int WM_DESTROYCLIPBOARD = 0x307;
            public const int WM_DRAWCLIPBOARD = 0x308;
            public const int WM_PAINTCLIPBOARD = 0x309;
            public const int WM_VSCROLLCLIPBOARD = 0x30A;
            public const int WM_SIZECLIPBOARD = 0x30B;
            public const int WM_ASKCBFORMATNAME = 0x30C;
            public const int WM_CHANGECBCHAIN = 0x30D;
            public const int WM_HSCROLLCLIPBOARD = 0x30E;
            public const int WM_QUERYNEWPALETTE = 0x30F;
            public const int WM_PALETTEISCHANGING = 0x310;
            public const int WM_PALETTECHANGED = 0x311;
            public const int WM_HOTKEY = 0x312;
            public const int WM_PRINT = 0x317;
            public const int WM_PRINTCLIENT = 0x318;
            public const int WM_APPCOMMAND = 0x319;
            public const int WM_THEMECHANGED = 0x31A;
            public const int WM_HANDHELDFIRST = 0x358;
            public const int WM_HANDHELDLAST = 0x35F;
            public const int WM_AFXFIRST = 0x360;
            public const int WM_AFXLAST = 0x37F;
            public const int WM_PENWINFIRST = 0x380;
            public const int WM_PENWINLAST = 0x38F;
        }

        #region MK_Controls
        //
        // Key State Masks for Mouse Messages
        //
        protected class MK_Controls
        {
            public const int MK_LBUTTON = 0x0001;
            public const int MK_RBUTTON = 0x0002;
            public const int MK_SHIFT = 0x0004;
            public const int MK_CONTROL = 0x0008;
            public const int MK_MBUTTON = 0x0010;
        }

        #endregion

        public static IntPtr MouseMove(int ID, int x, int y)
        {
            IntPtr hWnd = ID2Handle(ID);
            if (hWnd != IntPtr.Zero)
                return SendMessage(hWnd, WMmessages.WM_MOUSEMOVE, IntPtr.Zero, MakeLParam(x, y));
            return IntPtr.Zero;
        }

        // modified to send to window handle below mouse
        public static void SendMouseClick(int ID, bool Left)
        {
            IntPtr hWnd = ID2Handle(ID);
            if (hWnd != IntPtr.Zero)
            {
                //MouseMove(ID, x, y);
                Point p = WinControl.winGetPosition(ID);
                Point m = Control.MousePosition;
                int x = m.X - p.X - 3;
                int y = m.Y - p.Y - 25;

                if (x > 0 && y > 0)
                {
                    if (Left)
                    {
                        SendMessage(hWnd, WMmessages.WM_LBUTTONDOWN, (IntPtr)MK_Controls.MK_LBUTTON, MakeLParam(x, y));
                        SendMessage(hWnd, WMmessages.WM_LBUTTONUP, (IntPtr)MK_Controls.MK_LBUTTON, MakeLParam(x, y));
                    }
                    else
                    {
                        SendMessage(hWnd, WMmessages.WM_RBUTTONDOWN, (IntPtr)MK_Controls.MK_RBUTTON, MakeLParam(x, y));
                        SendMessage(hWnd, WMmessages.WM_RBUTTONUP, (IntPtr)MK_Controls.MK_RBUTTON, MakeLParam(x, y));

                    }
                }
            }
        }
        #endregion

        // special function for sending the mouse postion
        // into the message system of another window that would normally
        // rely on focus/direct interaction
        public static void MoveMouseOnWindow(int ID)
        {
            Point p = WinControl.winGetPosition(ID);
            Point m = Control.MousePosition;
            int x = m.X - p.X - 3;
            int y = m.Y - p.Y - 25;

            if (x > 0 && y > 0)
            {
                WinControl.MouseMove(ID, x, y);
            }
        }
    */

    }
}
