﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using IniControl;

namespace AO_Map_Client
{
    internal class Settings
    {
        static IniReader ini = new IniReader(Application.CommonAppDataPath + "\\AOMap Settings.ini");

        /// <summary>
        /// Option Settings
        /// </summary>
        public static string OwnerName
        {
            get { return ini.ReadString("Option Settings", "OwnerName"); }
            set { ini.Write("Option Settings", "OwnerName", value); }
        }

        public static string CoreAdress
        {
            get { return ini.ReadString("Option Settings", "CoreAddress", "127.0.0.1"); }
            set { ini.Write("Option Settings", "CoreAddress", value); }
        }
        public static bool Grab_D2_Follows_Map_Client
        {
            get { return ini.ReadBoolean("Option Settings", "D2 Follows Map Client", false); }
            set { ini.Write("Option Settings", "D2 Follows Map Client", value); }
        }
        public static string CoreLocation
        {
            get { return ini.ReadString("Option Settings", "CoreLocation", ""); }
            set { ini.Write("Option Settings", "CoreLocation", value); }
        }

        /// <summary>
        /// Map UI Settings
        /// </summary>
        public static int Opacity_Map
        {
            get { return ini.ReadInteger("Map UI Settings", "Opacity Map", 2); }
            set { ini.Write("Map UI Settings", "Opacity Map", value); }
        }

        public static int Opacity_PlayerPos
        {
            get { return ini.ReadInteger("Map UI Settings", "Opacity PlayerPos", 10); }
            set { ini.Write("Map UI Settings", "Opacity PlayerPos", value); }
        }

        public static bool Display_Map
        {
            get { return ini.ReadBoolean("Map UI Settings", "Display Map", true); }
            set { ini.Write("Map UI Settings", "Display Map", value); }
        }

        public static bool Display_PlayerPos
        {
            get { return ini.ReadBoolean("Map UI Settings", "Display PlayerPos", true); }
            set { ini.Write("Map UI Settings", "Display PlayerPos", value); }
        }

        /// <summary>
        /// Map Drawing Style Settings
        /// </summary>
        public static bool Perspective
        {
            get { return ini.ReadBoolean("Map Drawing Settings", "Perspective", true); }
            set { ini.Write("Map Drawing Settings", "Perspective", value); }
        }

        public static bool Dithering
        {
            get { return ini.ReadBoolean("Map Drawing Settings", "Dithering", true); }
            set { ini.Write("Map Drawing Settings", "Dithering", value); }
        }

        public static bool Invert
        {
            get { return ini.ReadBoolean("Map Drawing Settings", "Invert", false); }
            set { ini.Write("Map Drawing Settings", "Invert", value); }
        }

        public static bool MultiTone
        {
            get { return ini.ReadBoolean("Map Drawing Settings", "MultiTone", true); }
            set { ini.Write("Map Drawing Settings", "MultiTone", value); }
        }

        public static bool PrecisionDrawing
        {
            get { return ini.ReadBoolean("Map Drawing Settings", "PrecisionDrawing", false); }
            set { ini.Write("Map Drawing Settings", "PrecisionDrawing", value); }
        }

    }
}
