﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace AO_Map_Client
{
    public partial class Form_Debug : Form
    {
        Form parent;

        public Form_Debug(Form Parent)
        {
            InitializeComponent();
            parent = Parent;
        }

        // Adds a green or red string to the debug window
        public void Debug(string debugString, bool Good)
        {
            try
            {
                textBoxDebug.AppendText("\n" + (Good == true ? "1 " : "0 ") + debugString + " "); // " " so selection doesn't continue to next line :)

                // selects the added text and colors it apropriatly
                textBoxDebug.Select(textBoxDebug.Text.Length - debugString.Length - 1, debugString.Length);
                textBoxDebug.SelectionColor = Good == true ? Color.Lime : Color.Red;

                // puts caret @ end of text : )
                textBoxDebug.Select(textBoxDebug.Text.Length, 1);
                textBoxDebug.DeselectAll();
                textBoxDebug.ScrollToCaret();
            }
            catch { }
        }

        // Limits the lines in the debug window
        private void textBoxDebug_TextChanged(object sender, EventArgs e)
        {
            int Limiter = 11; // amount of lines you want plus 1 // 12 fits box perfectly // 11 looks good // 21 is plenty

            if (textBoxDebug.Lines.Length >= Limiter)
            {

                int difference = textBoxDebug.Lines.Length - Limiter;
                string[] strings = new string[Limiter - 1];

                for (int i = 0; i <= textBoxDebug.Lines.Length - difference - 2; i++)
                {
                    strings[i] = textBoxDebug.Lines[i + difference + 1];
                }

                textBoxDebug.Clear();

                // adds the new shortened strings back to the debug window with their colors
                foreach (string s in strings)
                {
                    Debug(s.Substring(2, s.Length - 3), s.Remove(1) == "1" ? true : false);
                }

            }
        }

        // Moves the debug window to the bottom of the parent window :)
        public void WindowMan()
        {
            this.Location = new Point(parent.Location.X, parent.Location.Y + parent.Height);
        }

    }
}
