﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AO_Map_Client
{
    public partial class Form_Options : Form
    {
        bool saved;
        public Form_Options()
        {
            InitializeComponent();
        }

        private void Form_Options_Load(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void buttonSaveAndExit_Click(object sender, EventArgs e)
        {
            SaveAndExit();
        }

        private void Form_Options_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!saved)
            {
                if (MessageBox.Show("Exit without saving?", "Exit", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.Dispose();
                }
                else
                {
                    SaveAndExit();
                }
            }
        }

        private void LoadSettings()
        {
            trackBarMap.Value = Settings.Opacity_Map;
            trackBarPlayerLocation.Value = Settings.Opacity_PlayerPos;

            // transparencies
            labelValMap.Text = trackBarMap.Value + "0%";
            labelValPlayerLocation.Text = trackBarPlayerLocation.Value + "0%";

            // display
            buttonMap.ForeColor = Settings.Display_Map ? Color.Green : Color.Maroon;
            buttonPlayerLocation.ForeColor = Settings.Display_PlayerPos ? Color.Green : Color.Maroon;

            // connection
            textBoxOwnerName.Text = Settings.OwnerName;
            textBoxCoreAddress.Text = Settings.CoreAdress;

            // grabbing
            comboBoxGrabType.Text = Settings.Grab_D2_Follows_Map_Client ? "D2 Follows Map Client" : "Map Client Follows D2";

            textBoxCoreLocation.Text = Settings.CoreLocation;
        }

        private void SaveAndExit()
        {
            // transparencies
            Settings.Opacity_Map = trackBarMap.Value;
            Settings.Opacity_PlayerPos = trackBarPlayerLocation.Value;

            // display
            Settings.Display_Map = buttonMap.ForeColor.Equals(Color.Green) 
                ? true : false;
            Settings.Display_PlayerPos = buttonPlayerLocation.ForeColor.Equals(Color.Green) 
                ? true : false;

            // connection
            Settings.OwnerName = textBoxOwnerName.Text;
            Settings.CoreAdress = textBoxCoreAddress.Text;

            // grabbing
            Settings.Grab_D2_Follows_Map_Client = comboBoxGrabType.Text.Contains("D2 Follows Map Client") ? true : false;

            Settings.CoreLocation = textBoxCoreLocation.Text;

            saved = true;
            this.Dispose();
        }

        private void trackBarMap_Scroll(object sender, EventArgs e)
        {
            labelValMap.Text = trackBarMap.Value + "0%";
        }

        private void trackBarPlayerLocation_Scroll(object sender, EventArgs e)
        {
            labelValPlayerLocation.Text = trackBarPlayerLocation.Value + "0%";
        }

        private void buttonMap_Click(object sender, EventArgs e)
        {
            buttonMap.ForeColor = buttonMap.ForeColor.Equals(Color.Green) 
                ? Color.Maroon : Color.Green;
        }

        private void buttonPlayerLocation_Click(object sender, EventArgs e)
        {
            buttonPlayerLocation.ForeColor = buttonPlayerLocation.ForeColor.Equals(Color.Green) 
                ? Color.Maroon : Color.Green;
        }

        private void textBoxCoreLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fb = new FolderBrowserDialog();
            fb.RootFolder = Environment.SpecialFolder.Desktop;
            DialogResult result = fb.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBoxCoreLocation.Text = fb.SelectedPath;
            }
        }
    }
}
