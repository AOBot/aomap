﻿namespace AO_Map_Client
{
    partial class Form_Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trackBarPlayerLocation = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxTransparencies = new System.Windows.Forms.GroupBox();
            this.labelValPlayerLocation = new System.Windows.Forms.Label();
            this.labelValMap = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.trackBarMap = new System.Windows.Forms.TrackBar();
            this.buttonSaveAndExit = new System.Windows.Forms.Button();
            this.groupBoxDisplay = new System.Windows.Forms.GroupBox();
            this.buttonPlayerLocation = new System.Windows.Forms.Button();
            this.buttonMap = new System.Windows.Forms.Button();
            this.groupBoxConnection = new System.Windows.Forms.GroupBox();
            this.labelCoreAddress = new System.Windows.Forms.Label();
            this.textBoxCoreAddress = new System.Windows.Forms.TextBox();
            this.textBoxOwnerName = new System.Windows.Forms.TextBox();
            this.labelOwnerName = new System.Windows.Forms.Label();
            this.comboBoxGrabType = new System.Windows.Forms.ComboBox();
            this.groupBoxGrabbing = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBoxCoreLocation = new System.Windows.Forms.GroupBox();
            this.textBoxCoreLocation = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarPlayerLocation)).BeginInit();
            this.groupBoxTransparencies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMap)).BeginInit();
            this.groupBoxDisplay.SuspendLayout();
            this.groupBoxConnection.SuspendLayout();
            this.groupBoxGrabbing.SuspendLayout();
            this.groupBoxCoreLocation.SuspendLayout();
            this.SuspendLayout();
            // 
            // trackBarPlayerLocation
            // 
            this.trackBarPlayerLocation.Location = new System.Drawing.Point(135, 63);
            this.trackBarPlayerLocation.Name = "trackBarPlayerLocation";
            this.trackBarPlayerLocation.Size = new System.Drawing.Size(116, 45);
            this.trackBarPlayerLocation.TabIndex = 0;
            this.trackBarPlayerLocation.Scroll += new System.EventHandler(this.trackBarPlayerLocation_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label1.Location = new System.Drawing.Point(93, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Map";
            // 
            // groupBoxTransparencies
            // 
            this.groupBoxTransparencies.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxTransparencies.Controls.Add(this.labelValPlayerLocation);
            this.groupBoxTransparencies.Controls.Add(this.label2);
            this.groupBoxTransparencies.Controls.Add(this.trackBarPlayerLocation);
            this.groupBoxTransparencies.Controls.Add(this.labelValMap);
            this.groupBoxTransparencies.Controls.Add(this.trackBarMap);
            this.groupBoxTransparencies.Controls.Add(this.label1);
            this.groupBoxTransparencies.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxTransparencies.ForeColor = System.Drawing.Color.DodgerBlue;
            this.groupBoxTransparencies.Location = new System.Drawing.Point(12, 280);
            this.groupBoxTransparencies.Name = "groupBoxTransparencies";
            this.groupBoxTransparencies.Size = new System.Drawing.Size(300, 115);
            this.groupBoxTransparencies.TabIndex = 2;
            this.groupBoxTransparencies.TabStop = false;
            this.groupBoxTransparencies.Text = "Transparencies";
            // 
            // labelValPlayerLocation
            // 
            this.labelValPlayerLocation.AutoSize = true;
            this.labelValPlayerLocation.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValPlayerLocation.Location = new System.Drawing.Point(257, 67);
            this.labelValPlayerLocation.Name = "labelValPlayerLocation";
            this.labelValPlayerLocation.Size = new System.Drawing.Size(15, 16);
            this.labelValPlayerLocation.TabIndex = 5;
            this.labelValPlayerLocation.Text = "%";
            // 
            // labelValMap
            // 
            this.labelValMap.AutoSize = true;
            this.labelValMap.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValMap.Location = new System.Drawing.Point(257, 24);
            this.labelValMap.Name = "labelValMap";
            this.labelValMap.Size = new System.Drawing.Size(15, 16);
            this.labelValMap.TabIndex = 4;
            this.labelValMap.Text = "%";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label2.Location = new System.Drawing.Point(18, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Player Location";
            // 
            // trackBarMap
            // 
            this.trackBarMap.Location = new System.Drawing.Point(135, 24);
            this.trackBarMap.Name = "trackBarMap";
            this.trackBarMap.Size = new System.Drawing.Size(116, 45);
            this.trackBarMap.TabIndex = 2;
            this.trackBarMap.Scroll += new System.EventHandler(this.trackBarMap_Scroll);
            // 
            // buttonSaveAndExit
            // 
            this.buttonSaveAndExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveAndExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.buttonSaveAndExit.Font = new System.Drawing.Font("Trebuchet MS", 11.25F);
            this.buttonSaveAndExit.Location = new System.Drawing.Point(198, 503);
            this.buttonSaveAndExit.Name = "buttonSaveAndExit";
            this.buttonSaveAndExit.Size = new System.Drawing.Size(114, 30);
            this.buttonSaveAndExit.TabIndex = 3;
            this.buttonSaveAndExit.Text = "Save and Exit";
            this.buttonSaveAndExit.UseVisualStyleBackColor = false;
            this.buttonSaveAndExit.Click += new System.EventHandler(this.buttonSaveAndExit_Click);
            // 
            // groupBoxDisplay
            // 
            this.groupBoxDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxDisplay.Controls.Add(this.buttonPlayerLocation);
            this.groupBoxDisplay.Controls.Add(this.buttonMap);
            this.groupBoxDisplay.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxDisplay.ForeColor = System.Drawing.Color.DodgerBlue;
            this.groupBoxDisplay.Location = new System.Drawing.Point(12, 197);
            this.groupBoxDisplay.Name = "groupBoxDisplay";
            this.groupBoxDisplay.Size = new System.Drawing.Size(300, 67);
            this.groupBoxDisplay.TabIndex = 6;
            this.groupBoxDisplay.TabStop = false;
            this.groupBoxDisplay.Text = "Display (Green=True)";
            // 
            // buttonPlayerLocation
            // 
            this.buttonPlayerLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.buttonPlayerLocation.Font = new System.Drawing.Font("Trebuchet MS", 11.25F);
            this.buttonPlayerLocation.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonPlayerLocation.Location = new System.Drawing.Point(152, 24);
            this.buttonPlayerLocation.Name = "buttonPlayerLocation";
            this.buttonPlayerLocation.Size = new System.Drawing.Size(142, 30);
            this.buttonPlayerLocation.TabIndex = 8;
            this.buttonPlayerLocation.Text = "Player Location";
            this.buttonPlayerLocation.UseVisualStyleBackColor = false;
            this.buttonPlayerLocation.Click += new System.EventHandler(this.buttonPlayerLocation_Click);
            // 
            // buttonMap
            // 
            this.buttonMap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.buttonMap.Font = new System.Drawing.Font("Trebuchet MS", 11.25F);
            this.buttonMap.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonMap.Location = new System.Drawing.Point(6, 24);
            this.buttonMap.Name = "buttonMap";
            this.buttonMap.Size = new System.Drawing.Size(142, 30);
            this.buttonMap.TabIndex = 7;
            this.buttonMap.Text = "Map";
            this.buttonMap.UseVisualStyleBackColor = false;
            this.buttonMap.Click += new System.EventHandler(this.buttonMap_Click);
            // 
            // groupBoxConnection
            // 
            this.groupBoxConnection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxConnection.Controls.Add(this.labelCoreAddress);
            this.groupBoxConnection.Controls.Add(this.textBoxCoreAddress);
            this.groupBoxConnection.Controls.Add(this.textBoxOwnerName);
            this.groupBoxConnection.Controls.Add(this.labelOwnerName);
            this.groupBoxConnection.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxConnection.ForeColor = System.Drawing.Color.DodgerBlue;
            this.groupBoxConnection.Location = new System.Drawing.Point(12, 12);
            this.groupBoxConnection.Name = "groupBoxConnection";
            this.groupBoxConnection.Size = new System.Drawing.Size(300, 91);
            this.groupBoxConnection.TabIndex = 9;
            this.groupBoxConnection.TabStop = false;
            this.groupBoxConnection.Text = "Connection";
            // 
            // labelCoreAddress
            // 
            this.labelCoreAddress.AutoSize = true;
            this.labelCoreAddress.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelCoreAddress.Location = new System.Drawing.Point(31, 58);
            this.labelCoreAddress.Name = "labelCoreAddress";
            this.labelCoreAddress.Size = new System.Drawing.Size(96, 20);
            this.labelCoreAddress.TabIndex = 7;
            this.labelCoreAddress.Text = "Core Address";
            // 
            // textBoxCoreAddress
            // 
            this.textBoxCoreAddress.Location = new System.Drawing.Point(133, 55);
            this.textBoxCoreAddress.Name = "textBoxCoreAddress";
            this.textBoxCoreAddress.Size = new System.Drawing.Size(161, 25);
            this.textBoxCoreAddress.TabIndex = 6;
            // 
            // textBoxOwnerName
            // 
            this.textBoxOwnerName.Location = new System.Drawing.Point(133, 24);
            this.textBoxOwnerName.Name = "textBoxOwnerName";
            this.textBoxOwnerName.Size = new System.Drawing.Size(161, 25);
            this.textBoxOwnerName.TabIndex = 4;
            // 
            // labelOwnerName
            // 
            this.labelOwnerName.AutoSize = true;
            this.labelOwnerName.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelOwnerName.Location = new System.Drawing.Point(9, 27);
            this.labelOwnerName.Name = "labelOwnerName";
            this.labelOwnerName.Size = new System.Drawing.Size(118, 20);
            this.labelOwnerName.TabIndex = 3;
            this.labelOwnerName.Text = "D2 Owner Name";
            // 
            // comboBoxGrabType
            // 
            this.comboBoxGrabType.DropDownHeight = 60;
            this.comboBoxGrabType.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxGrabType.FormattingEnabled = true;
            this.comboBoxGrabType.IntegralHeight = false;
            this.comboBoxGrabType.Items.AddRange(new object[] {
            "D2 Follows Map Client",
            "Map Client Follows D2"});
            this.comboBoxGrabType.Location = new System.Drawing.Point(133, 24);
            this.comboBoxGrabType.Name = "comboBoxGrabType";
            this.comboBoxGrabType.Size = new System.Drawing.Size(161, 26);
            this.comboBoxGrabType.TabIndex = 10;
            // 
            // groupBoxGrabbing
            // 
            this.groupBoxGrabbing.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxGrabbing.Controls.Add(this.label3);
            this.groupBoxGrabbing.Controls.Add(this.comboBoxGrabType);
            this.groupBoxGrabbing.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxGrabbing.ForeColor = System.Drawing.Color.DodgerBlue;
            this.groupBoxGrabbing.Location = new System.Drawing.Point(12, 411);
            this.groupBoxGrabbing.Name = "groupBoxGrabbing";
            this.groupBoxGrabbing.Size = new System.Drawing.Size(300, 67);
            this.groupBoxGrabbing.TabIndex = 6;
            this.groupBoxGrabbing.TabStop = false;
            this.groupBoxGrabbing.Text = "Grabbing";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label3.Location = new System.Drawing.Point(37, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Follow Mode";
            // 
            // groupBoxCoreLocation
            // 
            this.groupBoxCoreLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxCoreLocation.Controls.Add(this.textBoxCoreLocation);
            this.groupBoxCoreLocation.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxCoreLocation.ForeColor = System.Drawing.Color.DodgerBlue;
            this.groupBoxCoreLocation.Location = new System.Drawing.Point(12, 119);
            this.groupBoxCoreLocation.Name = "groupBoxCoreLocation";
            this.groupBoxCoreLocation.Size = new System.Drawing.Size(300, 62);
            this.groupBoxCoreLocation.TabIndex = 12;
            this.groupBoxCoreLocation.TabStop = false;
            this.groupBoxCoreLocation.Text = "Core Location  (For running functions)";
            // 
            // textBoxCoreLocation
            // 
            this.textBoxCoreLocation.Location = new System.Drawing.Point(0, 24);
            this.textBoxCoreLocation.Name = "textBoxCoreLocation";
            this.textBoxCoreLocation.Size = new System.Drawing.Size(300, 25);
            this.textBoxCoreLocation.TabIndex = 7;
            this.textBoxCoreLocation.Click += new System.EventHandler(this.textBoxCoreLocation_Click);
            // 
            // Form_Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(324, 547);
            this.Controls.Add(this.groupBoxCoreLocation);
            this.Controls.Add(this.groupBoxGrabbing);
            this.Controls.Add(this.groupBoxConnection);
            this.Controls.Add(this.groupBoxDisplay);
            this.Controls.Add(this.buttonSaveAndExit);
            this.Controls.Add(this.groupBoxTransparencies);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form_Options";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Options";
            this.Load += new System.EventHandler(this.Form_Options_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Options_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarPlayerLocation)).EndInit();
            this.groupBoxTransparencies.ResumeLayout(false);
            this.groupBoxTransparencies.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMap)).EndInit();
            this.groupBoxDisplay.ResumeLayout(false);
            this.groupBoxConnection.ResumeLayout(false);
            this.groupBoxConnection.PerformLayout();
            this.groupBoxGrabbing.ResumeLayout(false);
            this.groupBoxGrabbing.PerformLayout();
            this.groupBoxCoreLocation.ResumeLayout(false);
            this.groupBoxCoreLocation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TrackBar trackBarPlayerLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxTransparencies;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar trackBarMap;
        private System.Windows.Forms.Button buttonSaveAndExit;
        private System.Windows.Forms.Label labelValPlayerLocation;
        private System.Windows.Forms.Label labelValMap;
        private System.Windows.Forms.GroupBox groupBoxDisplay;
        private System.Windows.Forms.Button buttonPlayerLocation;
        private System.Windows.Forms.Button buttonMap;
        private System.Windows.Forms.GroupBox groupBoxConnection;
        private System.Windows.Forms.Label labelOwnerName;
        private System.Windows.Forms.TextBox textBoxOwnerName;
        private System.Windows.Forms.Label labelCoreAddress;
        private System.Windows.Forms.TextBox textBoxCoreAddress;
        private System.Windows.Forms.ComboBox comboBoxGrabType;
        private System.Windows.Forms.GroupBox groupBoxGrabbing;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBoxCoreLocation;
        private System.Windows.Forms.TextBox textBoxCoreLocation;
    }
}