﻿namespace AO_Map_Client
{
    partial class Form_Main
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.buttonConnect = new System.Windows.Forms.Button();
            this.panelControl = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonToggleOverlay = new System.Windows.Forms.Button();
            this.buttonStartBot = new System.Windows.Forms.Button();
            this.buttonFollower = new System.Windows.Forms.Button();
            this.labelWindowId = new System.Windows.Forms.Label();
            this.buttonShowD2Window = new System.Windows.Forms.Button();
            this.labelArea = new System.Windows.Forms.Label();
            this.buttonForceRedraw = new System.Windows.Forms.Button();
            this.panelShowHideConnection = new System.Windows.Forms.Panel();
            this.labelLocation = new System.Windows.Forms.Label();
            this.panelConnection = new System.Windows.Forms.Panel();
            this.checkBoxPrecisionDrawing = new System.Windows.Forms.CheckBox();
            this.checkBoxDrawing = new System.Windows.Forms.CheckBox();
            this.checkBoxFollower = new System.Windows.Forms.CheckBox();
            this.checkBoxDither = new System.Windows.Forms.CheckBox();
            this.buttonOptions = new System.Windows.Forms.Button();
            this.checkBoxPerspective = new System.Windows.Forms.CheckBox();
            this.checkBoxInvert = new System.Windows.Forms.CheckBox();
            this.checkBoxMultiTone = new System.Windows.Forms.CheckBox();
            this.buttonDebug = new System.Windows.Forms.Button();
            this.checkBoxCharacterCenterLock = new System.Windows.Forms.CheckBox();
            this.panelShowHide = new System.Windows.Forms.Panel();
            this.labelShowHide = new System.Windows.Forms.Label();
            this.pictureBoxMap = new System.Windows.Forms.PictureBox();
            this.timerSearchD2 = new System.Windows.Forms.Timer(this.components);
            this.timerCpuUse = new System.Windows.Forms.Timer(this.components);
            this.panelControl.SuspendLayout();
            this.panelConnection.SuspendLayout();
            this.panelShowHide.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMap)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonConnect
            // 
            this.buttonConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonConnect.Location = new System.Drawing.Point(227, 59);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(65, 20);
            this.buttonConnect.TabIndex = 0;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // panelControl
            // 
            this.panelControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelControl.Controls.Add(this.button2);
            this.panelControl.Controls.Add(this.button1);
            this.panelControl.Controls.Add(this.buttonExit);
            this.panelControl.Controls.Add(this.buttonToggleOverlay);
            this.panelControl.Controls.Add(this.buttonStartBot);
            this.panelControl.Controls.Add(this.buttonFollower);
            this.panelControl.Controls.Add(this.labelWindowId);
            this.panelControl.Controls.Add(this.buttonShowD2Window);
            this.panelControl.Controls.Add(this.labelArea);
            this.panelControl.Controls.Add(this.buttonForceRedraw);
            this.panelControl.Controls.Add(this.panelShowHideConnection);
            this.panelControl.Controls.Add(this.labelLocation);
            this.panelControl.Controls.Add(this.panelConnection);
            this.panelControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl.Location = new System.Drawing.Point(0, 359);
            this.panelControl.Name = "panelControl";
            this.panelControl.Size = new System.Drawing.Size(606, 90);
            this.panelControl.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(143, 15);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(30, 24);
            this.button2.TabIndex = 31;
            this.button2.Text = "clr";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(105, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 24);
            this.button1.TabIndex = 30;
            this.button1.Text = "tele";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExit.Location = new System.Drawing.Point(3, 3);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(20, 20);
            this.buttonExit.TabIndex = 26;
            this.buttonExit.Text = "X";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonToggleOverlay
            // 
            this.buttonToggleOverlay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonToggleOverlay.Location = new System.Drawing.Point(191, 32);
            this.buttonToggleOverlay.Name = "buttonToggleOverlay";
            this.buttonToggleOverlay.Size = new System.Drawing.Size(102, 20);
            this.buttonToggleOverlay.TabIndex = 25;
            this.buttonToggleOverlay.Text = "Overlay Hide";
            this.buttonToggleOverlay.UseVisualStyleBackColor = true;
            this.buttonToggleOverlay.Click += new System.EventHandler(this.buttonToggleOverlay_Click);
            // 
            // buttonStartBot
            // 
            this.buttonStartBot.Location = new System.Drawing.Point(113, 67);
            this.buttonStartBot.Name = "buttonStartBot";
            this.buttonStartBot.Size = new System.Drawing.Size(75, 20);
            this.buttonStartBot.TabIndex = 29;
            this.buttonStartBot.Text = "Start Bot";
            this.buttonStartBot.UseVisualStyleBackColor = true;
            this.buttonStartBot.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonFollower
            // 
            this.buttonFollower.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFollower.Location = new System.Drawing.Point(191, 6);
            this.buttonFollower.Name = "buttonFollower";
            this.buttonFollower.Size = new System.Drawing.Size(102, 20);
            this.buttonFollower.TabIndex = 24;
            this.buttonFollower.Text = "Follower Off";
            this.buttonFollower.UseVisualStyleBackColor = true;
            this.buttonFollower.Click += new System.EventHandler(this.buttonHoldD2_Click);
            // 
            // labelWindowId
            // 
            this.labelWindowId.AutoSize = true;
            this.labelWindowId.ForeColor = System.Drawing.Color.White;
            this.labelWindowId.Location = new System.Drawing.Point(4, 49);
            this.labelWindowId.Name = "labelWindowId";
            this.labelWindowId.Size = new System.Drawing.Size(66, 13);
            this.labelWindowId.TabIndex = 28;
            this.labelWindowId.Text = "Window ID: ";
            // 
            // buttonShowD2Window
            // 
            this.buttonShowD2Window.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonShowD2Window.Location = new System.Drawing.Point(3, 67);
            this.buttonShowD2Window.Name = "buttonShowD2Window";
            this.buttonShowD2Window.Size = new System.Drawing.Size(104, 20);
            this.buttonShowD2Window.TabIndex = 27;
            this.buttonShowD2Window.Text = "Show D2 Window";
            this.buttonShowD2Window.UseVisualStyleBackColor = true;
            this.buttonShowD2Window.Click += new System.EventHandler(this.buttonShowD2Window_Click);
            // 
            // labelArea
            // 
            this.labelArea.AutoSize = true;
            this.labelArea.ForeColor = System.Drawing.Color.White;
            this.labelArea.Location = new System.Drawing.Point(35, 3);
            this.labelArea.Name = "labelArea";
            this.labelArea.Size = new System.Drawing.Size(35, 13);
            this.labelArea.TabIndex = 21;
            this.labelArea.Text = "Area: ";
            // 
            // buttonForceRedraw
            // 
            this.buttonForceRedraw.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonForceRedraw.Location = new System.Drawing.Point(191, 67);
            this.buttonForceRedraw.Name = "buttonForceRedraw";
            this.buttonForceRedraw.Size = new System.Drawing.Size(102, 20);
            this.buttonForceRedraw.TabIndex = 14;
            this.buttonForceRedraw.Text = "Force Redraw";
            this.buttonForceRedraw.UseVisualStyleBackColor = true;
            this.buttonForceRedraw.Click += new System.EventHandler(this.buttonForceRedraw_Click);
            // 
            // panelShowHideConnection
            // 
            this.panelShowHideConnection.BackColor = System.Drawing.Color.Maroon;
            this.panelShowHideConnection.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelShowHideConnection.Location = new System.Drawing.Point(299, 0);
            this.panelShowHideConnection.Name = "panelShowHideConnection";
            this.panelShowHideConnection.Size = new System.Drawing.Size(10, 90);
            this.panelShowHideConnection.TabIndex = 11;
            this.panelShowHideConnection.Click += new System.EventHandler(this.panelShowHideConnection_Click);
            // 
            // labelLocation
            // 
            this.labelLocation.AutoSize = true;
            this.labelLocation.ForeColor = System.Drawing.Color.White;
            this.labelLocation.Location = new System.Drawing.Point(3, 26);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(67, 13);
            this.labelLocation.TabIndex = 3;
            this.labelLocation.Text = "Location xy: ";
            // 
            // panelConnection
            // 
            this.panelConnection.Controls.Add(this.checkBoxPrecisionDrawing);
            this.panelConnection.Controls.Add(this.checkBoxDrawing);
            this.panelConnection.Controls.Add(this.checkBoxFollower);
            this.panelConnection.Controls.Add(this.checkBoxDither);
            this.panelConnection.Controls.Add(this.buttonOptions);
            this.panelConnection.Controls.Add(this.checkBoxPerspective);
            this.panelConnection.Controls.Add(this.checkBoxInvert);
            this.panelConnection.Controls.Add(this.checkBoxMultiTone);
            this.panelConnection.Controls.Add(this.buttonDebug);
            this.panelConnection.Controls.Add(this.checkBoxCharacterCenterLock);
            this.panelConnection.Controls.Add(this.buttonConnect);
            this.panelConnection.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelConnection.Location = new System.Drawing.Point(309, 0);
            this.panelConnection.Name = "panelConnection";
            this.panelConnection.Size = new System.Drawing.Size(297, 90);
            this.panelConnection.TabIndex = 20;
            // 
            // checkBoxPrecisionDrawing
            // 
            this.checkBoxPrecisionDrawing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxPrecisionDrawing.AutoSize = true;
            this.checkBoxPrecisionDrawing.ForeColor = System.Drawing.Color.White;
            this.checkBoxPrecisionDrawing.Location = new System.Drawing.Point(117, 70);
            this.checkBoxPrecisionDrawing.Name = "checkBoxPrecisionDrawing";
            this.checkBoxPrecisionDrawing.Size = new System.Drawing.Size(111, 17);
            this.checkBoxPrecisionDrawing.TabIndex = 29;
            this.checkBoxPrecisionDrawing.Text = "Precision Drawing";
            this.checkBoxPrecisionDrawing.UseVisualStyleBackColor = true;
            this.checkBoxPrecisionDrawing.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxDrawing
            // 
            this.checkBoxDrawing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxDrawing.AutoSize = true;
            this.checkBoxDrawing.ForeColor = System.Drawing.Color.White;
            this.checkBoxDrawing.Location = new System.Drawing.Point(117, 3);
            this.checkBoxDrawing.Name = "checkBoxDrawing";
            this.checkBoxDrawing.Size = new System.Drawing.Size(65, 17);
            this.checkBoxDrawing.TabIndex = 28;
            this.checkBoxDrawing.Text = "Drawing";
            this.checkBoxDrawing.UseVisualStyleBackColor = true;
            this.checkBoxDrawing.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxFollower
            // 
            this.checkBoxFollower.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxFollower.AutoSize = true;
            this.checkBoxFollower.ForeColor = System.Drawing.Color.White;
            this.checkBoxFollower.Location = new System.Drawing.Point(117, 25);
            this.checkBoxFollower.Name = "checkBoxFollower";
            this.checkBoxFollower.Size = new System.Drawing.Size(56, 17);
            this.checkBoxFollower.TabIndex = 27;
            this.checkBoxFollower.Text = "Follow";
            this.checkBoxFollower.UseVisualStyleBackColor = true;
            this.checkBoxFollower.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxDither
            // 
            this.checkBoxDither.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxDither.AutoSize = true;
            this.checkBoxDither.ForeColor = System.Drawing.Color.White;
            this.checkBoxDither.Location = new System.Drawing.Point(117, 49);
            this.checkBoxDither.Name = "checkBoxDither";
            this.checkBoxDither.Size = new System.Drawing.Size(54, 17);
            this.checkBoxDither.TabIndex = 26;
            this.checkBoxDither.Text = "Dither";
            this.checkBoxDither.UseVisualStyleBackColor = true;
            this.checkBoxDither.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // buttonOptions
            // 
            this.buttonOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOptions.Location = new System.Drawing.Point(227, 7);
            this.buttonOptions.Name = "buttonOptions";
            this.buttonOptions.Size = new System.Drawing.Size(65, 20);
            this.buttonOptions.TabIndex = 23;
            this.buttonOptions.Text = "Options";
            this.buttonOptions.UseVisualStyleBackColor = true;
            this.buttonOptions.Click += new System.EventHandler(this.buttonOptions_Click);
            // 
            // checkBoxPerspective
            // 
            this.checkBoxPerspective.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxPerspective.AutoSize = true;
            this.checkBoxPerspective.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxPerspective.ForeColor = System.Drawing.Color.White;
            this.checkBoxPerspective.Location = new System.Drawing.Point(5, 3);
            this.checkBoxPerspective.Name = "checkBoxPerspective";
            this.checkBoxPerspective.Size = new System.Drawing.Size(106, 17);
            this.checkBoxPerspective.TabIndex = 9;
            this.checkBoxPerspective.Text = "Map Perspective";
            this.checkBoxPerspective.UseVisualStyleBackColor = true;
            this.checkBoxPerspective.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxInvert
            // 
            this.checkBoxInvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxInvert.AutoSize = true;
            this.checkBoxInvert.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxInvert.ForeColor = System.Drawing.Color.White;
            this.checkBoxInvert.Location = new System.Drawing.Point(58, 26);
            this.checkBoxInvert.Name = "checkBoxInvert";
            this.checkBoxInvert.Size = new System.Drawing.Size(53, 17);
            this.checkBoxInvert.TabIndex = 12;
            this.checkBoxInvert.Text = "Invert";
            this.checkBoxInvert.UseVisualStyleBackColor = true;
            this.checkBoxInvert.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBoxMultiTone
            // 
            this.checkBoxMultiTone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxMultiTone.AutoSize = true;
            this.checkBoxMultiTone.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxMultiTone.ForeColor = System.Drawing.Color.White;
            this.checkBoxMultiTone.Location = new System.Drawing.Point(35, 49);
            this.checkBoxMultiTone.Name = "checkBoxMultiTone";
            this.checkBoxMultiTone.Size = new System.Drawing.Size(76, 17);
            this.checkBoxMultiTone.TabIndex = 21;
            this.checkBoxMultiTone.Text = "Multi Tone";
            this.checkBoxMultiTone.UseVisualStyleBackColor = true;
            this.checkBoxMultiTone.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // buttonDebug
            // 
            this.buttonDebug.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDebug.Location = new System.Drawing.Point(227, 33);
            this.buttonDebug.Name = "buttonDebug";
            this.buttonDebug.Size = new System.Drawing.Size(65, 20);
            this.buttonDebug.TabIndex = 7;
            this.buttonDebug.Text = "Debug";
            this.buttonDebug.UseVisualStyleBackColor = true;
            this.buttonDebug.Click += new System.EventHandler(this.buttonDebug_Click);
            // 
            // checkBoxCharacterCenterLock
            // 
            this.checkBoxCharacterCenterLock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxCharacterCenterLock.AutoSize = true;
            this.checkBoxCharacterCenterLock.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxCharacterCenterLock.ForeColor = System.Drawing.Color.White;
            this.checkBoxCharacterCenterLock.Location = new System.Drawing.Point(5, 70);
            this.checkBoxCharacterCenterLock.Name = "checkBoxCharacterCenterLock";
            this.checkBoxCharacterCenterLock.Size = new System.Drawing.Size(106, 17);
            this.checkBoxCharacterCenterLock.TabIndex = 13;
            this.checkBoxCharacterCenterLock.Text = "Center Character";
            this.checkBoxCharacterCenterLock.UseVisualStyleBackColor = true;
            this.checkBoxCharacterCenterLock.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // panelShowHide
            // 
            this.panelShowHide.BackColor = System.Drawing.Color.Maroon;
            this.panelShowHide.Controls.Add(this.labelShowHide);
            this.panelShowHide.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelShowHide.Location = new System.Drawing.Point(0, 349);
            this.panelShowHide.Name = "panelShowHide";
            this.panelShowHide.Size = new System.Drawing.Size(606, 10);
            this.panelShowHide.TabIndex = 7;
            this.panelShowHide.Click += new System.EventHandler(this.panelShowHide_Click);
            // 
            // labelShowHide
            // 
            this.labelShowHide.AutoSize = true;
            this.labelShowHide.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowHide.Location = new System.Drawing.Point(292, 0);
            this.labelShowHide.Name = "labelShowHide";
            this.labelShowHide.Size = new System.Drawing.Size(19, 7);
            this.labelShowHide.TabIndex = 8;
            this.labelShowHide.Text = "Hide";
            this.labelShowHide.Click += new System.EventHandler(this.labelShowHide_Click);
            // 
            // pictureBoxMap
            // 
            this.pictureBoxMap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(0)))));
            this.pictureBoxMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxMap.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxMap.Name = "pictureBoxMap";
            this.pictureBoxMap.Size = new System.Drawing.Size(606, 349);
            this.pictureBoxMap.TabIndex = 8;
            this.pictureBoxMap.TabStop = false;
            // 
            // timerSearchD2
            // 
            this.timerSearchD2.Enabled = true;
            this.timerSearchD2.Interval = 500;
            this.timerSearchD2.Tick += new System.EventHandler(this.timerSearchD2_Tick);
            // 
            // timerCpuUse
            // 
            this.timerCpuUse.Enabled = true;
            this.timerCpuUse.Interval = 500;
            this.timerCpuUse.Tick += new System.EventHandler(this.timerProcessStats_Tick);
            // 
            // Form_Main
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(606, 449);
            this.Controls.Add(this.pictureBoxMap);
            this.Controls.Add(this.panelShowHide);
            this.Controls.Add(this.panelControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(622, 487);
            this.Name = "Form_Main";
            this.Text = "AO Map";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(0)))));
            this.Load += new System.EventHandler(this.Form_Main_Load);
            this.Move += new System.EventHandler(this.Form_Main_Move);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_main_FormClosing);
            this.Resize += new System.EventHandler(this.Form_Main_Resize);
            this.panelControl.ResumeLayout(false);
            this.panelControl.PerformLayout();
            this.panelConnection.ResumeLayout(false);
            this.panelConnection.PerformLayout();
            this.panelShowHide.ResumeLayout(false);
            this.panelShowHide.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMap)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Panel panelControl;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.Panel panelShowHide;
        private System.Windows.Forms.Label labelShowHide;
        private System.Windows.Forms.Button buttonDebug;
        private System.Windows.Forms.CheckBox checkBoxPerspective;
        private System.Windows.Forms.Panel panelConnection;
        private System.Windows.Forms.Panel panelShowHideConnection;
        private System.Windows.Forms.CheckBox checkBoxInvert;
        private System.Windows.Forms.CheckBox checkBoxCharacterCenterLock;
        private System.Windows.Forms.Button buttonForceRedraw;
        private System.Windows.Forms.CheckBox checkBoxMultiTone;
        private System.Windows.Forms.Button buttonOptions;
        private System.Windows.Forms.Button buttonFollower;
        private System.Windows.Forms.Button buttonToggleOverlay;
        public System.Windows.Forms.PictureBox pictureBoxMap;
        private System.Windows.Forms.Label labelArea;
        private System.Windows.Forms.Timer timerSearchD2;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonShowD2Window;
        private System.Windows.Forms.Label labelWindowId;
        private System.Windows.Forms.CheckBox checkBoxDither;
        private System.Windows.Forms.Button buttonStartBot;
        private System.Windows.Forms.Timer timerCpuUse;
        private System.Windows.Forms.CheckBox checkBoxDrawing;
        private System.Windows.Forms.CheckBox checkBoxFollower;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox checkBoxPrecisionDrawing;
    }
}

