﻿///
///
//  FORM OVERLAY Created By: CTS_AE
//  Use this to create an overlay on:
//      others forms or programs
//
///
///

using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BitmapControl;

namespace AO_Map_Client
{
    public partial class Form_OverlayTop : Form
    {

        Form_Main parent;
        public OverLayType Type;

        public enum OverLayType
        {
            Map,
            PlayerLocation,
            NPC,
            Object,
            Exit,
            TelePath
        }

        public Form_OverlayTop(Form_Main parent, OverLayType Type)
        {
            InitializeComponent();
            this.parent = parent;
            this.TopMost = true;
            this.Show();
            WinControl.SetWindowLayeredMode(this.Handle);
            this.Type = Type;
        }

        private void Form_Overlay_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = parent.FormBorderStyle;
            this.ControlBox = parent.ControlBox;
            this.Icon = parent.Icon;
            this.Width = parent.Width;
            this.Height = parent.Height;
            this.Location = parent.Location;
            this.Text = parent.Text;
            this.StartPosition = FormStartPosition.Manual;
            this.MinimumSize = parent.MinimumSize;
        }

        public Image OverlayImage
        {
            get { return pictureBox.BackgroundImage; }
            set { pictureBox.BackgroundImage = value; }
        }

    }
}
