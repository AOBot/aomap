﻿using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;
using System.IO;
using IniControl;

namespace AO_Map_Client
{
    public partial class Form_Main : Form
    {

 #region Globals
        AOMapClient mclient;
        AwesomoControllerClient Controller;
        MapDrawer gfx = new MapDrawer(Application.StartupPath + "\\Maps\\");
        CpuUsage cpu = new CpuUsage();

        Form_Debug debugControl;
        Form_OverlayTop topLayer;
        Form_Overlay posOverlay;
        Form_Overlay mapOverlay;
        Form_Overlay exitOverlay;
        public ArrayList Overlays = new ArrayList();
        FollowWindow FollowWin;
        GameLocation _Location;

        Caching Cache = new Caching();

        MapStruct currentMap ;
        float scaleFactor = 1;
        bool OverlayTop = true;
        bool D2Found = false;
        bool ClosedKnown = false;
        bool LevelChanged = false;
        bool Saving = false;
        bool OptionsNeeded = false; // so we know wether or not to continue while the options window is opened still
        bool Firsts = false;
        public int ID;
       
        delegate void AOMapClient_Handler_delegate(object sender, MapArgs myArgs);
        delegate void AwesomoControllerClient_Handler_delegate(object sender, PacketArgs myArgs);
        delegate void MapDrawer_Handler_delegate(object sender, Bitmap mapbmp);
        delegate void WindowStateChange(object sender, FormWindowState formWindowState);
        
        bool IsClientConnected
        {
            get
            {
                if (mclient != null)
                    return mclient.IsClientConnected;
                return false;
            }
        }
#endregion

 #region Mapclient Event Handler

        void mclient_MapChanged(object sender, MapArgs myArgs)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new AOMapClient_Handler_delegate(mclient_MapChanged), new object[] { sender, myArgs });
                return;
            }

            switch (myArgs._Event)
            {
                case MapArgs.Event.CORECONNECTED:
                    break;

                case MapArgs.Event.MAPADD:
                    // if true, then that means there's old data from a prior game
                    // since the core wont send a map twice ; )
                    if (Cache._MapCache.IsMapInCache(myArgs.Map.area))
                    {
                        Cache.ClearCache();
                    }
                    Cache._MapCache.AddMapToCache(myArgs.Map.area, myArgs.Map);
                    // Draw first map (town) since new games dont send a level change
                    // Draw if the level change was initiated before the map was recieved
                    if (Cache._MapCache.Count == 1 || LevelChanged == true)
                    {
                        DrawMap();
                    }
                    Debug("Map Recieved and Cached : " + myArgs.Map.area, true);
                    LevelChanged = false; // reset the bool
                    break;

                case MapArgs.Event.LEVELCHANGE:
                    LevelChanged = true;
                    if (Cache._MapCache.IsMapInCache(myArgs.Area))
                    {
                        currentMap = Cache._MapCache.GetMapFromCache(myArgs.Area);
                        DrawMap();
                        LevelChanged = false; // reset the bool
                    }
                    Debug("Level Changed to: " + myArgs.Area.ToString(), true);
                    labelArea.Text = "Area: " + myArgs.Area.ToString();

                    ArrayList exits = Cache._ExitCache.GetExitsFromCache(myArgs.Area);
                    if (exits != null)
                    {
                        exitOverlay.Exit = exits;
                        exitOverlay.pictureBox.Image = Drawing.orangeCrossSmall(
                            exits,
                            (int)currentMap.width, (int)currentMap.height,
                            checkBoxPerspective.Checked, 1);
                    }
                    else exitOverlay.pictureBox.Image = null;
                    break;
                    
                case MapArgs.Event.PLAYERPOS:
                    Point playerPosition = myArgs.Playerpos;
                    labelLocation.Text = "User Location: " + playerPosition.X.ToString() + ", " + playerPosition.Y.ToString();
                    if (checkBoxCharacterCenterLock.Checked && !checkBoxPerspective.Checked)
                    {

                        mapOverlay.Location = new Point(this.Location.X + pictureBoxMap.Width / 2 - playerPosition.X + 6, this.Location.Y + pictureBoxMap.Height / 2 - playerPosition.Y + 6);

                        posOverlay.OverlayImage = Drawing.redcross(
                            8, 8, 16, 16, checkBoxPerspective.Checked, 1);
                        posOverlay.Location = new Point(this.Location.X + pictureBoxMap.Width/2, this.Location.Y + pictureBoxMap.Height/2);
                        exitOverlay.Location = new Point(this.Location.X + pictureBoxMap.Width / 2 - playerPosition.X + 6, this.Location.Y + pictureBoxMap.Height / 2 - playerPosition.Y + 6);
                    }
                    else if (checkBoxCharacterCenterLock.Checked && checkBoxPerspective.Checked)
                    {
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        ////////PERFECT!!! :D///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //mapOverlay.Location = new Point(this.Location.X + this.Width/2 - playerPosition.X + playerPosition.Y, this.Location.Y + this.Height/2 - playerPosition.Y/2 - playerPosition.X/2);

                        /// sorta close... : (
                        //                    mapOverlay.Location = new Point(
                        //this.Location.X + this.Width / 2 - playerPosition.X / sqr / 2 + playerPosition.Y / sqr / 2 - Convert.ToInt32(currentMap.width / sqr) / 2,
                        //this.Location.Y + this.Height / 2 - playerPosition.Y / sqr / 4 - playerPosition.X / sqr / 4 - Convert.ToInt32(currentMap.width / sqr) / 4);

                        int sqr = (int)Math.Sqrt(2);
                        mapOverlay.Location = new Point(
                            this.Location.X + this.Width / 2 - playerPosition.X + playerPosition.Y, 
                            this.Location.Y + this.Height / 2 - playerPosition.Y / 2 - playerPosition.X / 2);
                        /// newy =  sqrt( normx^2 + normy^2 )
                        /// newx = oldx/sqrt(2) + oldy/sqrt(2)
                        /// maybe these need to be reversed because...
                        /// we need to find the old instead of new?



                        posOverlay.OverlayImage = Drawing.redcross(
                            8, 8, 16, 16, checkBoxPerspective.Checked, 1);
                        posOverlay.Location = new Point(this.Location.X + 5 + pictureBoxMap.Width / 2, this.Location.Y + 10 + pictureBoxMap.Height / 2);
                        exitOverlay.Location = new Point(this.Location.X + pictureBoxMap.Width / 2 - playerPosition.X + 6, this.Location.Y + pictureBoxMap.Height / 2 - playerPosition.Y + 6);
                    }
                    else
                    {
                        if (posOverlay != null) posOverlay.calcWinPos();

                        if (mapOverlay != null) mapOverlay.calcWinPos();

                        foreach (Form_Overlay f in Overlays)
                        {
                            try
                            {
                                f.calcWinPos();
                            }
                            catch { Debug("FO in Main_Move", false); }
                        }

                        if (currentMap != null)
                        {
                            posOverlay.OverlayImage = Drawing.redcross(
                                playerPosition.X, playerPosition.Y,
                                (int)currentMap.width, (int)currentMap.height,
                                checkBoxPerspective.Checked, 1);
                        }
                    }
                    break;
                case MapArgs.Event.TELEPATH:
                    //myArgs.TPath
                    break;

                case MapArgs.Event.EXIT:
                    Cache._ExitCache.AddExitToCache(myArgs._Exit);

                    TextWriter tw = new StreamWriter(Settings.CoreLocation+"\\scripts\\temptele.lua");

                    tw.WriteLine("loadfile(\"scripts\\\\includes.lua\")()");
                    tw.WriteLine("IncludeSettings()");
                    tw.WriteLine("IncludeLibs()");
                    tw.WriteLine("IncludeBosses()");

                    tw.WriteLine("function temp()");
                    tw.WriteLine("	Ao:Chat(\"Warp Next Level\")");
                    tw.WriteLine("MoveToArea(AreaLevel."+myArgs._Exit.to.ToString()+")");
                    tw.WriteLine("end");

                    tw.Close();

                    Debug("exit from " + myArgs._Exit.from.ToString() + " to " + myArgs._Exit.to.ToString() + " at " + myArgs._Exit.location.X.ToString() + ", " + myArgs._Exit.location.Y.ToString(), true);
                    break;

                case MapArgs.Event.NPC:
                    Debug("npc " + myArgs._Npc.npcid.ToString() + " in area " + myArgs._Npc.area.ToString() + " at " + myArgs._Npc.location.X.ToString() + ", " + myArgs._Npc.location.X.ToString(),true);
                    Form_Overlay overlay = new Form_Overlay(this, pictureBoxMap.Size, Form_Overlay.OverLayType.NPC);
                    overlay.OverlayImage = Drawing.pointBox(
                        myArgs._Npc.npcid.ToString(), 
                        myArgs._Npc.location.X, myArgs._Npc.location.Y, 
                        pictureBoxMap.Width, pictureBoxMap.Height, 
                        Color.Red, 
                        checkBoxPerspective.Checked, 1);
                    Overlays.Add(overlay);
                    break;

                case MapArgs.Event.OBJECT:
                    Debug("object " + myArgs.Obj.objectid.ToString() + " in area " + myArgs.Obj.area.ToString() + " at " + myArgs.Obj.location.X.ToString() + ", " + myArgs.Obj.location.X.ToString(), true);
                    Debug("objectID: " + myArgs.Obj.objectid.ToString(), true);
                    break;

                case MapArgs.Event.SOCKETERROR:
                    try { mclient.Dispose(); } catch { }
                    mclient = null;
                    //MessageBox.Show(myArgs.Info, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Debug(myArgs.Info, false);
                    break;
            }
        }

        #endregion
        
 #region Controler Enum

        internal enum GameLocation
        {
            Login,
            Lobby,
            Chat,
            Ingame,
            None
        }

        #endregion

 #region Controler Event Handler, Start Function

        public void StartControler(bool controllerconnect)
        {
            _Location = GameLocation.Login;

            if (controllerconnect)
            {
                this.Controller = new AwesomoControllerClient(Settings.CoreAdress);
                this.Controller.PacketReceived += new AwesomoControllerClient_Handler(Controller_PacketReceived);

                if (!Controller.IsClientConnected)
                {
                    Controller.CreateNewControllerClient(Settings.OwnerName);
                    Controller.Start();
                }
            }
        }

        private void Controller_PacketReceived(object sender, PacketArgs myArgs)
        {

            if (InvokeRequired)
            {
                BeginInvoke(new AwesomoControllerClient_Handler_delegate(Controller_PacketReceived), new object[] { sender, myArgs });
                return;
            }

            switch (myArgs.Event)
            {
                case AwesomoControllerClient.ControllerEventPackets.CONNECTION_SUCCESSFUL:
                    Debug("Controler Connected To Core Successfully", true);
                    break;

                case AwesomoControllerClient.ControllerEventPackets.ALREADY_CONNECTED:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.BNET_AUTH_RESPONSE:
                    _Location = GameLocation.Login;
                    break;

                case AwesomoControllerClient.ControllerEventPackets.BNET_LOGON_RESPONSE:
                    // logon attemp
                    break;

                case AwesomoControllerClient.ControllerEventPackets.CANT_CONNECT:
                    // Received Ip Ban or cant connect to bnet; Wrong ip?
                    break;

                case AwesomoControllerClient.ControllerEventPackets.TEMPORARY_IP_BAN:
                    //Received Ip Ban
                    break;

                case AwesomoControllerClient.ControllerEventPackets.CHARACTER_SELECT:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.LOGON_SUCCESS:
                    // Acctually lobby entry? so game exit aswell
                    Debug("In Lobby", true);
                    labelArea.Text = "Area: Lobby";
                    Cache.ClearCache();
                    OverlayHide(true);
                    _Location = GameLocation.Lobby;
                    break;

                case AwesomoControllerClient.ControllerEventPackets.JOIN_CHANNEL:
                    _Location = GameLocation.Chat;
                    break;

                case AwesomoControllerClient.ControllerEventPackets.JOIN_GAME_SUCCESSFUL:
                    Debug("Joined Game", true);
                    OverlayHide(false);
                    if (Cache._MapCache.Count > 0) DrawMap();
                    break;

                case AwesomoControllerClient.ControllerEventPackets.FAILED_TO_JOIN:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.GAME_ALREADY_EXISTS:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.GAME_DOES_NOT_EXISTS:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.CONNECTED_GAME_PROXY:
                    Cache.ClearCache();
                    _Location = GameLocation.Ingame;
                    break;

                case AwesomoControllerClient.ControllerEventPackets.EXIT_GAME:
                    Debug("Game Exited", false);
                    break;

                case AwesomoControllerClient.ControllerEventPackets.ITEM_MESSAGE:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.STATUS_MESSAGE:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.CHICKEN_EXIT:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.DEATH:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.SOCKET_ERROR:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.HOT_IP:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.DIA_CLONE:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.CHAT_EVENT:  
                // Your friend %ACCOUNT% entered a Diablo II Lord of Destruction game called %GAMENAME%.
                    // myArgs.Account + myArgs.CharName + myArgs.Message
                    
                /*
                    if (Profile.Follower)
                    {
                        if (_Location == Location.Chat || _Location == Location.Lobby)
                        {
                            if (myArgs.Account == Profile.MasterAccount && myArgs.CharName == Profile.MasterCharacter)
                            {
                                if (myArgs.Message.Contains("Your friend " + Profile.MasterAccount + " entered a Diablo II Lord of Destruction game called"))
                                {
                                    string message = myArgs.Message;
                                    string gameName = message.Remove(0, (message.LastIndexOf(Convert.ToChar(" ")) + 1));
                                    gameName = gameName.Remove(gameName.Length - 1);
                                    JoinGame(gameName);
                                }
                            }
                        }
                    }
                    */ 
                    break;

                case AwesomoControllerClient.ControllerEventPackets.JOIN_MULE_GAME:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.MULEISFULL:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.FINISHED_MULE:
                    break;

                case AwesomoControllerClient.ControllerEventPackets.BEGIN_MULE:
                    break;
            }
        }

        #endregion

 #region Mapping

        private void DrawMap()
        {
            try
            {
                gfx.Draw(
                    currentMap, this.Width, this.Height,
                    checkBoxPerspective.Checked,
                    checkBoxDither.Checked,
                    checkBoxInvert.Checked,
                    checkBoxMultiTone.Checked,
                    scaleFactor);

                exitOverlay.pictureBox.Image = Drawing.orangeCrossSmall(
                    exitOverlay.Exit,
                    (int)currentMap.width, (int)currentMap.height,
                    checkBoxPerspective.Checked, 1);

                Debug("Map Drawn", true);
            }
            catch (Exception e) 
            { 
                Debug(e.Message, false);
                Debug("Thown in DrawMap()", false);
            }
        }

        private void MapDrawer_MapDrawn(object sender, Bitmap mapbmp)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MapDrawer_Handler_delegate(MapDrawer_MapDrawn), new object[] { sender, mapbmp });
                return;
            }

            mapOverlay.pictureBox.Image = mapbmp;

        }

        // All the map drawing style settings event if they are registered to this event
        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            DrawMap();
            SaveSettings();
        }
        
        // map force redraw, should become depreciated
        private void buttonForceRedraw_Click(object sender, EventArgs e)
        {
            DrawMap();
        }

        #endregion

 #region Form Functions

        // sends debug message to debug window
        public void Debug(string debugString, bool Good)
        {
            if (debugControl != null)
                debugControl.Debug(debugString, Good);
        }

        // toggles overlay visibility
        public void OverlayToggle()
        {
            if (OverlayTop)
            {
                foreach (Form_Overlay f in Overlays)
                {
                    try
                    {
                        if (f.Visible) f.Hide();
                    }
                    catch { Debug("FO in OverlayToggle OverlayTop True", false); }
                }
                mapOverlay.Visible = false;
                topLayer.Visible = false;
                posOverlay.Visible = false;
                OverlayTop = false;
                buttonToggleOverlay.Text = "Overlay Show";
            }
            else
            {
                
                    foreach (Form_Overlay f in Overlays)
                    {
                        try
                        {
                            if (!f.Visible) f.Show();
                        }
                        catch { Debug("FO in OverlayToggle OverlayTop False", false); }
                    }
                mapOverlay.Visible = true;
                topLayer.Visible = true;
                posOverlay.Visible = true;
                OverlayTop = true;
                buttonToggleOverlay.Text = "Overlay Hide";
            }
        }

        // hides/shows overlay
        public void OverlayHide(bool TrueFalse)
        {
            if (TrueFalse)
            {
                foreach (Form_Overlay f in Overlays)
                {
                    try
                    {
                        if (f != null) if (f.Visible) f.Hide();
                    }
                    catch { Debug("FO in OverlayHide True", false); }
                }
                topLayer.Visible = false;
                OverlayTop = false;
            }
            else
            {
                foreach (Form_Overlay f in Overlays)
                {
                    try
                    {
                        if (f != null) if (!f.Visible) f.Show();
                    }
                    catch { Debug("FO in OverlayHide False", false); }
                }
                topLayer.Visible = true;
                OverlayTop = true;
            }
        }

        // Win follower toggle
        public void FollowToggle()
        {
            if (FollowWin == null)
            {
                FollowWin = new FollowWindow(this, ID);
                //// create event to watch d2's window state
                //trying to initialize if nulled instead of at the end of the follow toggle
                FollowWin.WinStateChange += new FollowWindow.WindowStateChange(FollowWin_WinStateChange);
                FollowWin.Start();

                this.Size = new Size(WinControl.winGetSize(ID).Width, WinControl.winGetSize(ID).Height + 100);
                WinControl.ShowWindow(WinControl.ID2Handle(ID), WinControl.WindowShowStyle.Show);
                buttonFollower.Text = "Follower Off";
          }
            else if (FollowWin.Follow.ThreadState == System.Threading.ThreadState.Stopped)
            {
                FollowWin.Toggle(true);

                this.Size = new Size(WinControl.winGetSize(ID).Width, WinControl.winGetSize(ID).Height + 100);
                WinControl.ShowWindow(WinControl.ID2Handle(ID), WinControl.WindowShowStyle.Show);
                buttonFollower.Text = "Follower Off";
            }
            else
            {
                FollowWin.Toggle(false); // puts it into a stopped state not aborted; odd
                buttonFollower.Text = "Follower On";
            }
            while (this.Size != new Size(WinControl.winGetSize(ID).Width, WinControl.winGetSize(ID).Height + 100))
            {
                System.Threading.Thread.Sleep(150);
                this.Size = new Size(WinControl.winGetSize(ID).Width, WinControl.winGetSize(ID).Height + 100);
                Debug("Adjusting size to D2 window size. Window is proabbly animating to restore.", false);
            }
        }

        public void ConnectToCore()
        {
            StartControler(true);

            if (!String.IsNullOrEmpty(Settings.OwnerName))
            {
                if (String.IsNullOrEmpty(Settings.CoreAdress))
                    mclient = new AOMapClient();
                else
                    mclient = new AOMapClient(Settings.CoreAdress);

                mclient.MapChanged += new AOMapClient_Handler(mclient_MapChanged);
                mclient.CreateNewMapClient(Settings.OwnerName);
                if (IsClientConnected)
                {
                    mclient.Start();
                    mapOverlay.pictureBox.Image = gfx.DrawAcsiiArt("!Connected.txt", this.Width, this.Height - panelControl.Height);
                    Debug("Map Client Connected To Core : )", true);
                }

                OptionsNeeded = false;
            }
            else
            {
                OverlayHide(true);
                if (FollowWin != null)
                {
                    FollowWin.Toggle(false);

                }
                MessageBox.Show("Fill Out The Owner Name Of Diablo 2");
                Debug("No Owner Name", false);
                Form_Options o = new Form_Options();
                o.Disposed += new EventHandler(o_Disposed);
                o.TopMost = true;
                o.Show();

                OptionsNeeded = true;
                Firsts = true;
            }
            SaveSettings();
        }

        private void SaveSettings()
        {
            if (!Saving)
            {
                Settings.Perspective = checkBoxPerspective.Checked;
                Settings.Dithering = checkBoxDither.Checked;
                Settings.Invert = checkBoxInvert.Checked;
                Settings.MultiTone = checkBoxMultiTone.Checked;
                Settings.PrecisionDrawing = checkBoxPrecisionDrawing.Checked;
            }
        }

        private void LoadSettings()
        {
            Saving = true;
            checkBoxPerspective.Checked = Settings.Perspective;
            checkBoxDither.Checked = Settings.Dithering;
            checkBoxInvert.Checked = Settings.Invert;
            checkBoxMultiTone.Checked = Settings.MultiTone;
            checkBoxPrecisionDrawing.Checked = Settings.PrecisionDrawing;
            Saving = false;
        }

        private void ShowD2()
        {
            WinControl.ShowWindow(WinControl.ID2Handle(ID), WinControl.WindowShowStyle.ShowNormal);
            ///important!///WinControl.winSetTopMost(ID);
            WinControl.winSetShowV2(ID);
            //WinControl.ShowWindow(WinControl.ID2Handle(ID), WinControl.WindowShowStyle.Show);
            Debug("D2 Shown", true);
        }

        #endregion

 #region Form Events
        // Constructor
        public Form_Main()
        {
            InitializeComponent();
            LoadSettings();
            
            debugControl = new Form_Debug(this);
            gfx.MapDrawn += new MapDrawer_Handler(MapDrawer_MapDrawn);
        }

        //// follow window event trigger
        void FollowWin_WinStateChange(object sender, FormWindowState formWindowState)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new WindowStateChange(FollowWin_WinStateChange), new object[] { sender, formWindowState });
                return;
            }
            if (formWindowState == FormWindowState.Normal && this.WindowState != FormWindowState.Normal)
                this.Activate();
            if (this.WindowState != formWindowState) 
                this.WindowState = formWindowState;
        }

        private void Form_Main_Load(object sender, EventArgs e)
        {
            // Overlay Setup
             // Top Layer
            topLayer = new Form_OverlayTop(this, Form_OverlayTop.OverLayType.PlayerLocation);
             // map
            mapOverlay = new Form_Overlay(this, pictureBoxMap.Size, Form_Overlay.OverLayType.Map);
            mapOverlay.calcWinPos();
            Overlays.Add(mapOverlay);
             // exit
            exitOverlay = new Form_Overlay(this, pictureBoxMap.Size, Form_Overlay.OverLayType.Exit);
            Overlays.Add(exitOverlay);
             // pos
            posOverlay = new Form_Overlay(this, pictureBoxMap.Size, Form_Overlay.OverLayType.PlayerLocation);
            Overlays.Add(posOverlay);

            // set opacity
            posOverlay.Opacity = (double)Settings.Opacity_PlayerPos / (double)10;
            mapOverlay.Opacity = (double)Settings.Opacity_Map / (double)10;

            // load caches
            Cache.Load();
            if (Cache._MapCache.Count > 0)
            {
                Debug("Previous Maps Found (Loby Never Entered): Map Cache Loaded", true);
            }
            else
            {
                try
                {
                    mapOverlay.pictureBox.Image = gfx.DrawAcsiiArt("!Splash.txt", this.Width, this.Height);
                    Debug("Splash Screen Succesfully Drawn", true);
                }
                catch { Debug("Splash Screen Not Found, Missing maps folder?", false); }
            }

        }

        void o_Disposed(object sender, EventArgs e)
        {
            OverlayHide(false);
            if (FollowWin != null) FollowWin.Toggle(true);
            OptionsNeeded = false;
            if (Firsts)
            {
                FollowToggle();
                ShowD2();
                Firsts = false;
            }
        }

        private void frm_main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cache.Save();
            //if (mclient != null) mclient.Dispose();
            Environment.Exit(0);
        }

        #endregion

 #region Pannel Control
        private void panelShowHide_Click(object sender, EventArgs e)
        {
            ShowHidePannel();
        }

        private void labelShowHide_Click(object sender, EventArgs e)
        {
            ShowHidePannel();
        }

        /// show hide Connection pannel
        private void panelShowHideConnection_Click(object sender, EventArgs e)
        {
            if (panelConnection.Visible == true) panelConnection.Hide();
            else panelConnection.Show();
        }

        // show hide pannel control
        private void ShowHidePannel()
        {
            if (panelControl.Visible == true)
            {
                panelControl.Hide();
                labelShowHide.Text = "Show";
            }
            else
            {
                panelControl.Show();
                labelShowHide.Text = "Hide";
            }
        }
        #endregion

 #region GUI Events & Functions


        private void buttonConnect_Click(object sender, EventArgs e)
        {
            ConnectToCore();
        }

        private void buttonOptions_Click(object sender, EventArgs e)
        {
            OverlayHide(true);
            if (FollowWin != null)
                if (FollowWin.Following)
                {
                    FollowWin.Toggle(false);
                    FollowWin.QuickStop = true;
                }
            Form_Options o = new Form_Options();
            o.Disposed += new EventHandler(options_Disposed);
            o.ShowDialog();
            OverlayHide(false);
        }

        private void options_Disposed(object sender, EventArgs e)
        {
            posOverlay.Opacity = (double)Settings.Opacity_PlayerPos / (double)10;
            mapOverlay.Opacity = (double)Settings.Opacity_Map / (double)10;
            if (FollowWin != null) // start following again
                if (FollowWin.QuickStop) // if it started out as following
                    FollowWin.Toggle(true);

        }

        private void buttonHoldD2_Click(object sender, EventArgs e)
        {
            FollowToggle();
        }

        private void buttonToggleOverlay_Click(object sender, EventArgs e)
        {
            OverlayToggle();
        }

        private void buttonDebug_Click(object sender, EventArgs e)
        {
            if (debugControl.Visible == false) debugControl.Show();
            else debugControl.Hide();
            debugControl.WindowMan();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Cache.Save();
            //if (FollowWin != null) FollowWin.Toggle(false);
            //if (mclient != null) mclient.Stop();
            //if (Controller != null) Controller.Stop();
            Environment.Exit(0);
            //this.Dispose();
        }

        private void buttonShowD2Window_Click(object sender, EventArgs e)
        {
            ShowD2();
        }

        // Look for d2 process
        private void timerSearchD2_Tick(object sender, EventArgs e)
        {

            /// omfg what a beast.... looks for d2 process asks if they would like to grab it
            /// lots of bools insereted for loop issues, and first time shows and what not
            /// it sucked, but thank god it's done lol.
            Process[] processlist = Process.GetProcesses();
            //ArrayList foundProcess = new ArrayList();
            bool IDhit = false;
            foreach (Process i in processlist)
            {
                if (i.Id == ID) IDhit = true;
                if (i.ProcessName == "Game" && !D2Found)
                {
                    //foundProcess.Add(i);
                    ID = i.Id;
                    labelWindowId.Text = "Window ID: " + ID;
                    OverlayHide(true);
                    timerSearchD2.Stop();
                    D2Found = true;
                    if (MessageBox.Show(
                        "This Diablo II Window Has Been Assigned: " + ID + " \n Would You Like To Follow It?",
                        "Diablo II Has Been Found!",
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        ConnectToCore();
                        if (!OptionsNeeded)
                        {
                            ShowD2();
                            while (WinControl.GetWindowState(ID) != WinControl.WindowShowStyle.ShowNormal)
                            {
                                System.Threading.Thread.Sleep(250);
                            }
                            // appearntly delay helps the issues?
                            FollowToggle();
                        }
                    }
                    else
                    {
                        //
                    }
                    OverlayHide(false);
                    IDhit = true;
                    ClosedKnown = false;
                    timerSearchD2.Start();
                }
            }
            if (!IDhit)
            {
                D2Found = false;
                if (!ClosedKnown)
                {
                    ClosedKnown = true;
                    MessageBox.Show("D2 Closed!");
                    if (FollowWin.Following) FollowWin.Toggle(false);
                }

            }
        }

        #endregion

 #region Overlay Management

        private void Form_Main_Resize(object sender, EventArgs e)
        {
            topLayer.WindowState = this.WindowState;
            topLayer.Size = this.Size;
            topLayer.Location = this.Location;

            if (mapOverlay != null)
            {
                mapOverlay.Size = pictureBoxMap.Size;
                mapOverlay.calcWinPos();
            }

            foreach (Form_Overlay f in Overlays)
            {
                try
                {
                    f.Size = pictureBoxMap.Size;
                    f.calcWinPos();
                }
                catch { Debug("FO in Main_Resize", false); }
            }
        }

        private void Form_Main_Move(object sender, EventArgs e)
        {
            if (debugControl.Visible)
            {
                debugControl.WindowMan();
                //debugControl.Focus();
                WinControl.ShowWindow(debugControl.Handle, WinControl.WindowShowStyle.ShowNormal);
            }

            if (topLayer != null) topLayer.Location = this.Location;

            if (posOverlay != null) posOverlay.calcWinPos();

            if (mapOverlay != null) mapOverlay.calcWinPos();

            foreach (Form_Overlay f in Overlays)
            {
                try
                {
                    f.calcWinPos();
                }
                catch { Debug("FO in Main_Move", false); }
            }
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            //Controller.SendMsgToConClients("HEY!");
            Controller.RunLuaFile("libs\\Idle.lua", "Idle");
        }

        private void timerProcessStats_Tick(object sender, EventArgs e)
        {
            topLayer.Text = "AO Map      CPU Use: " + cpu.GetUsage().ToString();
        }

        private void checkBoxFollower_CheckedChanged(object sender, EventArgs e)
        {
            FollowToggle();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Controller.RunLuaFile("temptele.lua", "temp");
            ShowD2();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int radius = 20;

            TextWriter tw = new StreamWriter(Settings.CoreLocation+"\\scripts\\tempattack.lua");
            tw.WriteLine("loadfile(\"scripts\\\\includes.lua\")()");
            tw.WriteLine("IncludeSettings()");
            tw.WriteLine( "IncludeLibs()");
            tw.WriteLine("IncludeBosses()");
            tw.WriteLine(" ");
            tw.WriteLine("function temp()");
	        tw.WriteLine("Ao:Chat(\"Clearing Radius of "+radius+"\")");
            tw.WriteLine("ClearRadius(Ao.me.x, Ao.me.y, " + radius + ", true)");
	        tw.WriteLine("Ao:Chat(\"Done!\")");
            tw.WriteLine("end");
            tw.Close();
            Controller.RunLuaFile("tempattack.lua", "temp");
            ShowD2();
        }

    }
}
