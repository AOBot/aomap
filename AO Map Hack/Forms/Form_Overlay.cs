﻿///
///
//  FORM OVERLAY Created By: CTS_AE
//  Use this to create an overlay on:
//      others forms or programs
//
///
///

using System;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BitmapControl;

namespace AO_Map_Client
{
    public partial class Form_Overlay : Form
    {

        Form_Main parent;
        private ArrayList exit;
        public Size pictureboxSize;
        public OverLayType Type;

        public enum OverLayType
        {
            Map,
            PlayerLocation,
            NPC,
            Object,
            Exit,
            TelePath
        }

        public Form_Overlay(Form_Main parent, Size pictureboxSize, OverLayType Type)
        {
            this.Type = Type;

            this.Size = new Size(0, 0); 
            this.parent = parent;
            this.TopMost = true;
            this.Visible = true;
            this.pictureboxSize = pictureboxSize;

            InitializeComponent();

            WinControl.SetWindowLayeredMode(this.Handle);
            
            calcWinPos();
            this.Size = pictureboxSize;
            
            //WinControl.ShowWindow(this.Handle, WinControl.WindowShowStyle.Show);
        }

        /// <summary>
        /// You better be using exit strucs in this puppie : P
        /// </summary>
        public ArrayList Exit
        {
            get
            {
                return exit;
            }
            set
            {
                exit = value;
            }
        }

        // places into window bounds
        // as of now requires the parent form to be Form_Main
        public void calcWinPos()
        {
            this.Location = new Point
                (
                (parent.Location.X + ((parent.Width - parent.pictureBoxMap.Width) / 2)),
                // + 100 is the size of the lower pannel space in the main form.
                (parent.Location.Y + parent.Height - (parent.pictureBoxMap.Height + 100) - ((parent.Width - parent.pictureBoxMap.Width) / 2))
                );
        }

        public Image OverlayImage
        {
            get { return pictureBox.BackgroundImage; }
            set { pictureBox.BackgroundImage = value; }
        }

        private void Form_Overlay_FormClosing(object sender, FormClosingEventArgs e)
        {
            try { parent.Overlays.Remove(this); }
            catch { }
        }
        // hopefully will call the FormClosing and remove the object from the array list in Overlays
        new public void Dispose() 
        {
            try
            {
                this.Close();
            }
            catch (Exception) { }
        }

    }
}
